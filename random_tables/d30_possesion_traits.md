
## d30 последствий одержимости

[![sample posessed](http://fc06.deviantart.net/fs70/i/2011/253/9/5/possession__the_rite_by_jeuro85-d49g2ob.jpg)](http://jeuro85.deviantart.com/art/Possession-The-Rite-257731211)

И в продолжение нашей недели неблагих постов на PnPRPG, вот вам ещё 30 возможных последствий присутствия тёмных сущностей в вашем (или чужом) персонаже.

*Помимо того, мы выражаем искреннюю признательность [Metzihn](https://vk.com/meztihn), любезно предоставившему этот перевод.*

С оригиналом этого текста Вы можете ознакомиться [в блоге автора](http://wrathofzombie.wordpress.com/2013/10/15/the-corruption-from-demonic-possession-d30-table/)<!--more-->

---

Одержимость демоническими сущностями часто встречается во многих D&D/OSR-играх.

Сегодня я, в очередной раз, пересмотрел первые две части «Зловещих мертвецов» *(надо сказать что они – одни из моих любимых фильмов)* и понял что, даже если ты уже избавился от одержимости тёмной сущностью, этот интересный опыт не мог не оставить на тебе свой отпечаток.

В общем, если тот или иной персонаж в Вашей игре выжил после захвата его тела демоном, дайте ему сделать бросок d30 *(ну или d20+d10 – прим ред.)* и сверьтесь с  составленной мной таблицей.  
<strike>Поздравляем</strike> Теперь он несчастный обладатель того что там написано.

### Собственно таблица последствий

#### 1. Глаза Зверя
Ты можешь видеть как струится кровь внутри живущих.
#### 2. Когтистая длань
Когти на твоих руках наносят Сила+d4 урона.
#### 3. Гнилая плоть...
… и вонь гнилого мяса (-1 к харизме).
#### 4. Кривые, жёлтые, поломанные зубы
Ты клацаешь зубами при беседе.
#### 5. Седина
Ты выглядишь измождённым.
#### 6. Без отражения
Ты никогда больше не узришь себя.
#### 7. Иссушающий
Небольшие растения увядают от твоего касания.
#### 8. Пугало
Животные сторонятся тебя и будут атаковать, если придется.
#### 9. Бугимен
Дети клянутся, что видели тебя под своими кроватями!
#### 10. Касание праха
От твоего прикосновения вещи покрываются слоем праха на 1d4 часов.
#### 11. Бескровный
У тебя нет крови (попытки лечения с штрафом -1).
#### 12. Сосуд тревоги
Дети плачут, старики шарахаются от тебя. Люди тебя боятся, но ты не знаешь почему (-1 к харизме).
#### 13. Скопище рубцов
Твое тело покрыто ужасными шрамами (-1 к харизме).
#### 14. Лунатизм
Каждую ночь есть шанс 1 к 6, что ты что-то делал ночью, но не помнишь этого.
#### 15. Скопище нарывов
Сочащиеся язвы на твоем теле причиняют мучительную боль.
#### 16. Ночные кошмары
Ты просыпаешься с криками по ночам. Сделай спасбросок чтобы избежать этого.
#### 17. Краснокожий
Твоя кожа красная и горячая на ощупь.
#### 18. Лакомый кусочек
Вокруг тебя постоянно летают мухи.
#### 19. Колотун
Временами тебя трясет крупной дрожью. Есть шанс 2 из 6, что это проявится во время тяжёлых физических нагрузок (-1 к силе, ловкости и навыкам завязанных на них).
#### 20. Глаза Тьмы
Ты можешь видеть в абсолютной темноте. Даже магической.
#### 21. Слабое нутро
Тебя тошнит зеленой жижей, всегда когда ты взволнован.
#### 22. Кислотная кровь
Ты не можешь вылечиться с помощью магии. Каждый, кто ранит тебя получает 1d4 урона.
#### 23. Питие есть прах
Твое касание превращает воду в прах.
#### 24. Вековая тайна
Ты каким-то образом получил давно забытое, потерянное или запретное знание. Но то ты будешь с ним делать?
#### 25. Прореха разума
Демонам теперь проще проникать в твой разум (-1 к проверкам против одержимости).
#### 26. Читающий мысли
Ты слышишь, что думают люди, но не можешь различать их (проверки требующие концентрации получают штраф -1).
#### 27. Порченый
Есть 30% шанс подвергнуться воздействию заклинания Изгнание, произнесённого святошей.
#### 28. Нечестивый
Ты не можешь войти в священные места. Если все-таки окажешься там, то будешь получать 1d8 урона каждый раунд.
#### 29. Кровожад
Когда ты убиваешь кого-то, сделай успешную проверку характера, иначе продолжишь атаковать даже друзей.
#### 30. Воплощённое Злорадство
Тебе нравится наблюдать за за гнусными, подлыми и отвратительными действиями. Даже будучи в шоке, ты ликующе смеешься и гогочешь.
