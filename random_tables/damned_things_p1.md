**from:** http://hereticwerks.blogspot.ru/2011/08/

# Damned things p.1/ Кары небесные, ч.1

Snowflakes (1d20) inches in diameter, (1d6) inches thick fall like cold, sharp whirling bits of ice . (Treat as a Blade Barrier spell.)

Orange-flavored hailstones sizzle across the area doing little appreciable damage.

(1d100 x10) tons of butter fall from the sky, amazingly, no one is hurt.

(1d6) Aeroliths: stones falling from the sky, each one explodes on impact, sending shards across a wide area. Anyone caught out in the open suffers 1d6 damage.

Loud noises heard in the sky, but nothing is seen to happen.

Thick, inky-black rain that leaves a most disagreeable odor as it drizzles across the area like syrup.

Blue hailstones sleet across the region. They seem harmless.

Mahogany logs drop across a 3 square mile area.

Masses of charred beef and blood crash into a nearby area causing 2d4 damage to anyone caught out in the open.

Yellow substance including numerous globules of cobalt blue, also corpuscles of a pearly color that resemble starch falls during light rainfall.

Red sand containing 3d10 percent unidentifiable organic matter sifts into place over everything within a 62 mile radius, the sand continues to drizzle into this region for 3d4 hours.

(6d10) Tons of red mud falls from out of nowhere, causing landslides and some flooding.

Dust that is chocolate-colored and silky to the touch and slightly iridescent blows in from some indeterminate location.

Comingled reddish raindrops and gray sand fall for 2d4 turns.

Pink Rain falls for 1d4 hours.

Dust that is yellow-brown, with a tinge of pink coats everything within a three mile radius.

Jet-Black snow falls for 3d6 hours leaving drifts of 2d4 feet in depth across 1d4x100' radius.

Minute yellow organisms in the shape of arrows, coffee beans, horns and disks gently flutter to the ground where they wriggle helplessly for 2d6 turns until they shrivel, then melt into a nasty goo that leaves faint reddish stains.

Dust of a deep yellow-clay color from another world swirls across the terrain leaving a gritty residue.

Rain of blood lasts for 1d6 turns. (30%) chance that it revives 1d4 random vampires from the local dust.

Finely divided-up organic matter, a thick, viscous, putrid red matter that leaves stains.

Food-supplies from cargoes of super-vessels, wrecked in interplanetary traffic, come tumbling through the air like flaming meteors of questionable nutrition.

Orange-red hail of a peculiar substance consisting of red iron ocher, carbonate of lime, and unidentifiable organic matter covers a space roughly 200' square.

Red, white and blue hailstones pummel everyone within 600' radius for 1d4 damage/turn for 2d6 turns.

A gelatinous fungus, vaguely bowl-shaped object that has upon it a nap, similar to that of milled cloth. Upon removing this nap, a buff-colored, pulpy substance the consistency of soap is found. It has an offensive odor, and, upon exposure to the air, turns to a livid red and absorbs moisture from the air until it liquifies into a sticky mess. This thing was said to have fallen in conjunction with a brilliant light, but that might just have been coincidence.

Dead and dry fish that turn to blood when exposed to water.

Frothy, gelatinous spawn of frogs and fishes covers everything within a 300'x900' space.

Blue-green gelatinous matter that could be nostoc ( http://en.wikipedia.org/wiki/Nostoc ) drapes the surrounding 300' radius.

Organic substance with the consistency of beef, in the shape of flakes 1d4 inches in diameter falls like snow for 1d4 hours, leaving a bloody, horribly bad smelling mess once it begins to melt.

Grayish, nut-sized masses of a resinous substance that is odorless unless burned, in which case it then produces a sweet smelling gas that causes 1d4 damage to all who come into contact with the cloying pink smoke.

Originally, we developed the Tables of Damned Things for use with the Zalchis setting, but these tables are also pretty useful outside of Zalchis. We intend to adapt them for use in Wermspittle during the long dark nights of deepest winter, when the forces of misrule are at their most powerful. These tables can also be adapted to Call of Cthulhu, Savage Worlds, Trail of Cthulhu, or whatever you like.

For More on Charles Fort, please see our article at Old School Heretic.

For more ideas about how to integrate anomalous and Fortean phenomena into your game, we highly recommend the incredibly excellet Suppressed Transmission 1 and Suppressed Transmission 2 books by the one and only Ken Hite.
- See more at: http://hereticwerks.blogspot.ru/2011/08/following-is-designated-open-game_12.html#sthash.g5ptb8hA.dpuf