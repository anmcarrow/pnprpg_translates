**From:** http://thealexandrian.net/wordpress/4997/roleplaying-games/better-dungeon-maps-4-ceilings

**Title:** Улучшаем карты подземелий: ч.4 – Потолки (перевод)

![The Alexandrian splash](http://pnprpg.ru/wp-content/uploads/2014/08/the_alexandrian_splashes.png)

С оригиналом статьи Вы можете ознакомиться [здесь](http://thealexandrian.net/wordpress/4997/roleplaying-games/better-dungeon-maps-4-ceilings). 

---

Одна из целей стоящая перед этой серией постов –  поиск путей для удобного размещения как можно большего количества информации на карте подземелья.

Эта цель частично достижима при использовании легенды карты, однако мой поиск направлен ещё и на то чтобы вынести часть информации из легенды на саму карту *(упростив, таким образом, её использование)*. <!--more-->

#### Примером концепции служит вопрос высоты потолков

Несмотря на то что информация о высоте потолка как правило остаётся без внимания *(что становится проблемой когда персонажи внезапно хотят полетать)*, размещение её в легенде карты только усложняет её использование. 

Однако, я думаю что если разместить ту же информацию на самой карте, то это облегчит отсылки к ней и её практическое использование. 

*При условии, конечно, удобного метода записи этой информации.*

На топографических картах естественных пещер, разница высот и высота потолка обычно просто обозначаются цифрами.

Я обнаружил что для игровых карт, в чистом виде, такой метод приводит к перемешиванию цифр обозначения высоты и номеров локаций на карте, что создаёт путаницу. 

Использование различных размера, гарнитуры и цветов шрифта облегчает ситуацию, но конечный результат, на мой взгляд, всё-ещё выглядит недостаточно удобно и аккуратно.

##### Как бы-то ни было, предлагаю рассмотреть интересное решение вопроса из «Aeons & Auguries»:

![](http://www.thealexandrian.net/images/20110415.jpg)

Такой символ наглядно демонстрирует цифру и поддаётся быстрому опознанию (чётко демонстрирует свою однозначную функцию). 

В более общем смысле, если мы совместим этот наш символ высоты потолка с символами источников света, то нас уже появится способ снабдить каждое помещение своей собственной встроенной системой пометок.

##### Давайте попробуем реализовать эту идею:
![](http://www.thealexandrian.net/images/20110415b.jpg)

Возможно, здесь нужна некоторая донастройка наших символов освещения по размеру, дизайну или используемому в них шрифту для большей читабельности меток в целом. 

Однако, базовый принцип уже неплох и удобен в использовании:   
Глядя на наш план я без труда могу заметить что все три комнаты освещены свечами *(с 20-ти футовым радиусом*), и при максимальная высота потолка в зоне 12 возвышается над головами героев аж на 40 футов.

Если карта позволяет подобное, возможно также размещение небольшого куска легенды комнаты рядом с самой комнатой в купе с цифровыми пометками: 

![](http://www.thealexandrian.net/images/20110415c.jpg)

## Рельеф потолка
А что насчёт рельефа потолка? Есть ли способ отразить его на карте?

##### Вот такие топографические маркеры традиционно описывают форму потолка на картах пещер:

![](http://www.thealexandrian.net/images/20110415d.jpg)


##### Мы можем попробовать использовать упрощённую версию этого способа:

![](http://www.thealexandrian.net/images/20110415e.jpg)

Не то чтобы это было необходимо для каждой комнаты, но такой метод можно использовать там где все эти детали окажутся важны.

*To be continued...*

*[©The Alexandrian, 15.04.2011](http://thealexandrian.net/wordpress/4997/roleplaying-games/better-dungeon-maps-4-ceilings)*