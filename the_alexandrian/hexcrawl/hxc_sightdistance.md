**source:** http://thealexandrian.net/wordpress/25706/roleplaying-games/check-this-out-hexcrawl-sighting-distances

**title:** Дистанции видимости в Hexcrawl (перевод)

Rob Conley over at Bat in the Attic has posted some nice “rules of thumb” for long distance sighting in hexcrawls.

Meanwhile the Hydra’s Grotto points out that Conley is low-balling mountains in his post “Mountains and Mole-Hills“.

HexI’m posting this because (a) I find their posts useful and insightful, but also (b) because I think they’ve both missed the mark on mountains.

(Note that Conley uses a 5-mile hex, the Hydra’s Grotto uses a 6-mile hex, and I use a 12-mile hex. I use the 12-mile hex specifically because it simplifies away a lot of hex-to-hex sighting questions. But for the purposes of this post, I’m going to try to simplify things by converting back into actual measurements.)

When I did my series on Hexcrawls, I included a post on spotting distances. In that post, I included my rule of thumb that mountain ranges can be seen from 72 miles away, but I didn’t talk much about where I derived that value from: Basically, I did some quick research and determined that most mountain ranges have an average height of 3,000 feet or thereabouts. Notable peaks within a range will often be higher than that, but the average height of the range is 3,000 feet. And if you just do the calculations, you’ll discover that you can see an object 3,000 feet high from about 68 miles away (which I then rounded up to 6 hexes x 12 miles per hex = 72 miles). Or, if you reverse the math, I’m saying that at a distance of 72 miles you can see the occasional peak that’s up to 3,500 feet high in that range or thereabouts.

And so, for example, PCs can see the mountain range in my OD&D hexcrawl from about 6 hexes away. But there’s also a notable peak in hex L2 (the Stone Tooth from Forge of Fury) that’s not as high and can only be seen from 3 hexes away. And there’s also a volcano in hex K1 from which the smoke plume can be seen from much further away if it’s smoking. And a very tall peak of 10,000 feet in hex A1 which could theoretically be seen from 10 hexes away on a clear day.

The “clear day” proviso is an important on. The atmosphere itself will have an impact on your viewing distance (particularly for fine details) and haze can significantly decrease it:

 

Aerial Perspective - Joaquim Alves Gaspar

But I digress: Conley makes his calculations on the distance mountains can be seen from based on an elevation of 1,000 feet. That’s roughly the minimum height of a mountain and, therefore, way too low for seeing a range of mountains from the distance. Hydra’s Grotto aims equally high above average with an elevation of 5,000 feet, which actually exceeds the maximum height of some mountain ranges.

One thing I would pick up from both Conley and the Hydra’s Grotto is the idea of adding a specific mechanic for “finding a good place to sight from” that chews up some time but allows you to see a little further than you normally would. That idea is inherent in the guidelines for determining spotting distance based on the horizon and height (find a tree and you can see further), but hooking it as a specific, mechanical choice might encourage its presence in actual play.

12.02.2013