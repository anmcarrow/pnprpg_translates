source: http://thealexandrian.net/wordpress/25738/roleplaying-games/check-this-out-hexcrawl-tracks

Check This Out: Hexcrawl Tracks February 21st, 2013
Gray Wolf TracksSomething that I touched on only briefly in my Hexcrawl series was the subject of tracks: The system I use for encounter generation features the ability to create random encounters, lairs, and tracks. Random encounters provide immediate obstacles and interludes while traveling; lairs spontaneously generate new locations in the hexcrawl (organically building up material along well-traveled routes as the campaign develops); and tracks are a trail that can be followed to a point of interest.

What I didn’t really extrapolate on is the fact that the concept of “tracks” isn’t necessarily limited to hoof prints in the sod. In the wilderness exploration of the hexcrawl that sort of physical spoor is most likely very common, but the concept of “tracks” really generalizes to “clue”. For example, if I generated a result of “tracks” for bandits that might include a merchant caravan in panicked disarray due to their latest highway robbery; the dead body of a bandit that was critically wounded and abandoned; a bolt-hole containing documents implicating the mayor of a local village in collusion with the bandits; and so forth.

Roger the GS recently posted “Almost Encounters: Sights, Sounds, and Leavings” which breaks this sort of thing down into some useful categories:

Sights (“a pair of griffins flying across the sunset, many miles away”; “a brief red glow, sighted across a far-away ridge line”)
Sounds (“snatches of shouting and song down in the valley”)
Body Parts
Victims
Tracks
Smells and Vapors
Environment Damage
Intentional Markings
Check it out. There are a lot of great examples over there.