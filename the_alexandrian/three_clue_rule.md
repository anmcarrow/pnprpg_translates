**Title:** Правило Трёх Улик (перевод) 

**Связанные файлы:** three_clue_rule.md, revelations.md

![The Alexandrian alter-splash](http://pnprpg.ru/wp-content/uploads/2014/08/the_alexandrian_mystery.png)


С оригиналом статьи Вы можете ознакомиться [здесь](http://thealexandrian.net/wordpress/1118/roleplaying-games/three-clue-rule).

---

COLLECTED EDITION OF AN ESSAY BY JUSTIN ALEXANDER

*Небольшое эссе за авторством Джастина Александра*

	Mystery scenarios for roleplaying games have earned a reputation for turning into unmitigated disasters: The PCs will end up veering wildly off-course or failing to find a particular clue and the entire scenario will grind to a screeching halt or go careening off the nearest cliff. The players will become unsure of what they should be doing. The GM will feel as if they’ve done something wrong. And the whole evening will probably end in either boredom or frustration or both.

**Сценарии-загадки традиционно имеют в НРИ репутацию источника неизбывных проблем:** 
Персонажи, норовящие резко сменить изначально намеченный верный курс или провалить поиск важной улики.   
Истории, разваливающиеся, со скрежетом замирающие на полном ходу или, со всё увеличивающейся скоростью, несущиеся к ближайшим сюжетным рифам.   
Игроки не знающие что же им делать.  
Ведущие, остающиеся один на один с чувством собственной неправоты.   
И вечера, каждый из которых может закончиться скукой или разочарованием *(или и тем и другим одновременно)*.

	Here’s a typical example: When the PCs approach a murder scene they don’t search outside the house, so they never find the wolf tracks which transform into the tracks of a human. They fail the Search check to find the hidden love letters, so they never realize that both women were being courted by the same man. They find the broken crate reading DANNER’S MEATS, but rather than going back to check on the local butcher they spoke to earlier they decide to go stake out the nearest meat processing plant instead.

##### Вот вам наитипичнейшие примеры: 

- Если персонажи, находясь на месте убийства не обыщут окрестности дома, то они так и не найдут следов волка, превращающихся в следы человека. 
- Потом они провалят бросок на поиск спрятанных любовных посланий, и никогда не поймут что за обеими дамами ухаживал один и тот же кавалер.
- И наконец, найдя разломанный ящик с надписью *«Мясо Дэннера»*, они не пойдут сразу проверять ближайшую лавку  мясника, а вместо этого займутся поисками какого-нибудь местного мясокомбината.

		As a result of problems like these, many people reach an erroneous conclusion: Mystery scenarios in RPGs are a bad idea. In a typical murder mystery, for example, the protagonist is a brilliant detective. The players are probably not brilliant detectives. Therefore, mysteries are impossible.

В результате проблем подобных вышеописанным, многие люди склонны делать неверное умозаключение:   
*«Сценарии-загадки являются плохой идеей для НРИ в целом. В истории типичного загадочного убийства, например, главный герой – детектив высшей пробы. А наши игроки – ни разу нет. Таким образом, успешный сценарий-загадка невозможен.»*

		Or, as someone else once put it to me: “The players are not Sherlock Holmes.”
		
*Ну или, как мне сказал однажды кто-то – «Игроки не Шерлок Холмсы».*

![Sir Sherlock Holmes and others](http://static.zerochan.net/Sherlock.BBC.full.1140545.jpg)

	Although the conclusion is incorrect, there’s an element of truth in this. For example, in A Study in Scarlet, Sherlock Holmes is investigating the scene of a murder. He discovers a small pile of ashes in the corner of the room. He studies them carefully and is able to conclude that the ashes have come from a Trichinopoly cigar.

Несмотря на то что это умозаключение неверно, в нём есть доля правды.

> **Для примера:** В [«Этюде в багровых тонах»](http://www.lib.ru/AKONANDOJL/sh_scarl.txt), Шерлок Холмс, исследуя место убийства, находит небольшую кучку пепла в углу комнаты. Он аккуратно изучает кучку и приходит к выводу что это пепел от трихинопольской сигары.

	Now, let’s analyze how this relatively minor example of Holmesian deduction would play out at the game table:

	(1) The players would need to successfully search the room.

	(2) They would need to care enough about the ashes to examine them.

	(3) They would need to succeed at a skill check to identify them.

	(4) They would need to use that knowledge to reach the correct conclusion.

Теперь, давайте проанализируем чем отличается этот небольшой пример типичной дедукции Ш. Холмса от того что в такой сцене должно произойти за нашим игровым столом:

1. Игроки должны тщательно обыскать комнату;
2. Игроки должны достаточно аккуратно осмотреть пепел;
3. Игроки должны успешно пройти проверку соответствующего навыка для идентификации пепла;
4. Игроки должны верно интерпретировать полученные сведения для производства верного умозаключения.

		That’s four potential points of failure: The PCs could fail to search the room (either because the players don’t think to do it or because their skill checks were poor). The PCs could fail to examine the ashes (because they don’t think them important). The PCs could fail the skill check to identify them. The PCs could fail to make the correct deduction.

##### Каждый их этих этапов является потенциальной точкой отказа:
1. Персонажи могут завалить осмотр комнаты *(потому что игроки просто не знают что делать и/или потому что проверка навыков их персонажей недостаточно хороша)*;
2. Персонажи могут завалить осмотр пепла *(потому что просто не посчитают кучку пепла значимой)*;
3. Персонажи могут завалить проверку навыка для идентификации пепла.
4. Персонажи *(или игроки – прим. пер.)* могут попросту завалить процесс дедукции.
	
		If correctly understanding this clue is, in fact, essential to the adventure proceeding — if, for example, the PCs need to go to the nearest specialty cigar shop and start asking questions — then the clue serves as chokepoint: Either the PCs understand the clue or the PCs slam into a wall.

Если же они верно поймут эту улику *(по факту, играющую центральную роль в дальнейшем приключении)*, но потом направятся в ближайший магазин сигар и начнут задавать вопросы там, эта улика сыграет роль точки отказа. 

В таком случае, даже имея на руках расшифрованную улику, персонажи всё равно зайдут в тупик.

		Chokepoints in adventure design are always a big problem and need to be avoided, but we can see that when it comes to a mystery scenario the problem is much worse: Each clue is not just one chokepoint, it’s actually multiple chokepoints.

Такие точки отказа являются большой проблемой для дизайна приключений в целом и должны устраняться. 

Однако, как мы можем видеть,  у сценариев-загадок есть проблема и посерьёзнее: *Каждая улика рождает не одну точку отказа, а сразу несколько.*

		So the solution here is simple: Remove the chokepoints.

Решение этой проблему просто – нужно удалить все точки отказа.

## След из хлебных крошек

![Natural GUMSHOE](http://www.adagio.com/images2/custom_blends/34869.jpg)

	For the GUMSHOE system (used in The Esoterrorists, Fear Itself, and The Trail of Cthulhu), Robin D. Laws decided to get rid of the concept of needing to find clues. In each “scene” of an investigation scenario, there is a “clue”. It’s automatically assumed that the investigators will find this clue.

В своей игромеханике [«СЫЩИК»](http://ru.rpg.wikia.com/wiki/Gumshoe) *(используемой в «Эзотеррористах», «Страхе, как он есть» и «Ктулху»)*, [Робин Д. Лоуз](http://ru.rpg.wikia.com/wiki/Robin_Laws) решил совсем избавится от концепции поиска улик.    
В каждой «сцене» расследования присутствует некоторая «важная улика».    
*И она автоматически сдаётся сыщикам которые её ищут.* 

	This removes three of our four chokepoints, leaving only the necessity of using the clue to make the correct deduction (i.e., the deduction which moves you onto the next “scene” where the next clue can be imparted). And, in the case of the GUMSHOE system, even this step can be tackled mechanically (with the players committing points from their character’s skills to receive increasingly accurate “deductions” from the GM).

Это удаляет три из четырёх точек отказа, оставляя только дедукцию на основе найденной улики *(т.е. производства того самого умозаключения что продвигает вас к следующей «сцене», в которой вы получите следующую улику)*.   

И, в случае СЫЩИКА, даже этот шаг поддерживается игромеханикой *(которая позволяет игрокам тратить очки их специальных умений, разменивая их на получение верных результатов «дедукции» от ведущего)*.

	This is a mechanical solution to the problem. But while it may result in a game session which superficially follows the structure of a mystery story, I think it fails because it doesn’t particularly feel as if you’re playing a mystery.

Это – чисто механическое решение проблемы.  

Но, *несмотря на то что такой подход даст вам игровую встречу в точности повторяющую структуру сюжета-загадки*, я думаю что результат будет не менее провален, так как это не будет ощущаться настоящей игрой в разгадывание тайны.

	Laws’ fundamental mistake, I think, is in assuming that a mystery story is fundamentally about following a “bread crumb trail” of clues. Here’s a quote from a design essay on the subject:

Фундаментальная ошибка Лоуза, как мне кажется, состоит в том что он считает что истории-загадки в своей первооснове повествуют о *«мы идём по следу из хлебных крошек»*. 

##### Здесь уместно привести небольшую выдержку из его же эссе:

	I’d argue, first of all, that these fears are misplaced, and arise from a fundamental misperception. The trail of clues, or bread crumb plot, is not the story, and does not constitute a pre-scripted experience. What the PCs choose to do, and how they interact with each other as they solve the mystery, is the story. As mentioned in The Esoterrorist rules, we saw this at work during playtest, as all of the groups had very different experiences of the sample scenario, as each GM and player combo riffed in their own unique ways off the situations it suggested.

> «Я буду утверждать, прежде всего, что эти опасения напрасны, и проистекают из-за в корне неверного восприятия вопроса. След из улик или *«карта из хлебных крошек»* не являются самой историей и не дают нам какого-то предопределённого опыта. То как какой метод действий выбирают персонажи игроков, и то как они взаимодействуют друг с другом в процессе расследования загадки, вот это-то и есть история. Как я уже упоминал в правилах *«Эзотеррористов»*, во время проведения плейтестов мы обнаружили  что все игровые группы-тестеры получали очень раличный опыт при игре по одним и тем же сценариям, так как каждый ведущий и игрок создавали своя собственный, уникальный, способ для выхода из предложенных им ситуаций.»
[© R.D.Laws, Feb. 2007]((http://web.archive.org/web/20070219035419/http://www.dyingearth.com/pagexxfebruary2007.htm))

	But, in point of fact, this type of simplistic “A leads to B leads to C leads to D” plotting is not typical of the mystery genre. For a relatively simplistic counter-example, let’s return to Sherlock Holmes in A Study in Scarlet:

Однако, всё-таки, этот упрощённая *«А приводит к Б, которое приводит к В, и, в дальнейшем, к Г»* фабула далеко не типична для жанра тайн и загадок.

##### В качестве простого примера этого, давайте повторно обратимся к «Этюду в багровых тонах»:

	WATSON: “That seems simple enough,” said I; but how about the other man’s height?”
	HOLMES: “Why, the height of a man, in nine cases out of ten, can be told from the length of his stride. It is a simple calculation enough, though there is no use my boring you with figures. I had this fellow’s stride both on the clay outside and on the dust within. Then I had a way of checking my calculation. When a man writes on a wall, his instinct leads him to write above the level of his own eyes. Now that writing was just over six feet from the ground. It was child’s play.”

> **Уотсон:** Звучит достаточно просто, но что насчёт роста второго мужчины?    
> **Холмс:** Ну, рост человека, в девяти случаев из десяти, можно узнать по ширине его шага. Это очень несложно, но я не хочу утомлять вас вычислениями. Я измерил шаги убийцы и на глинистой дорожке и на пыльном полу комнаты. А потом мне предоставился случай проверить мои вычисления. Когда человек пишет на стене, он инстинктивно пишет на уровне собственных глаз. От пола до надписи на стене шесть футов. Одним словом, задачка для детей.

	This is just one small deduction in a much larger mystery, but you’ll note that Holmes has in fact gathered several clues, studied them, and then distilled a conclusion out of them. And this is, in fact, the typical structure of the mystery genre: The detective slowly gathers a body of evidence until, finally, a conclusion emerges. In the famous words of Holmes himself, “When you have eliminated the impossible, whatever remains, however improbable, must be the truth.”

Здесь совсем немного	дедукции и много тайны, но, как видите, Холмс просто собрал набор улик, изучил их и сделал выводы, основываясь на них. 

##### Вот это, по факту, и есть настоящая структура игры-загадки:
Детективы неспешно собирают доказательства до тех пор, пока, наконец, не смогут на их основании сделать вывод. 

> Или же, словами глубокоуважаемого Холмса: «Когда вы исключите невозможное, то что останется, скорее всего, будет правдой».

	What is true, however, is that in many cases it is necessary for many smaller deductions to be made in order for all of the evidence required to solve the mystery to be gathered. However, as the example from A Study in Scarlet demonstrates, even these smaller deductions can be based on a body of evidence and not just one clue in isolation.

Как бы то ни было, истина в том что, в большинстве случаев, для решения загадки требуется весьма небольшое количество дедукции по отношению к собранным уликам. 

Ну и, раз уж мы обратились за примером к «Этюду», даже такая небольшая дедукция должна быть основана на целом наборе улик, а не на одной единственной изолированной улике.

	This observation leads us, inexorably, to the solution we’ve been looking for.
	
*Это наблюдение неумолимо приводит нас к решению всех вышеописанных проблем.*

## Правило Трёх Улик

	Whenever you’re designing a mystery scenario, you should invariably follow the Three Clue Rule:

Когда вы создаёте ваш сценарий-загадку, вам обязательно нужно следовать Правилу Трёх Улик:

	For any conclusion you want the PCs to make, include at least three clues.

На каждое умозаключение, которое, *по Вашему*, должны будут сделать игроки, включайте в игру три улики.

	Why three? Because the PCs will probably miss the first; ignore the second; and misinterpret the third before making some incredible leap of logic that gets them where you wanted them to go all along.

##### Почему именно три?

Потому что персонажи, скорее всего, потеряют первую, проигнорируют вторую, и не уяснят смысл третьей, после чего у них случится внезапное озарение, которое позволит им понять то что вы хотите чтобы они поняли.

	I’m kidding, of course. But if you think of each clue as a plan (the PCs will find A, conclude B, and go to C), then when you have three clues you’ve not only got a plan — you’ve also got two backup plans. And when you realize that your plans never survive contact with the players, the need for those backup plans becomes clear.

*Я шучу, конечно.* 

Однако, если вы воспринимаете каждую из этих улик как плана *«Персонажи находят А, понимают Б и двигаются к В»*, то в случае трёх улик у вас есть не просто план, у вас есть ещё два запасных плана.

И когда выяснится что ваш план не пережил контакта с вашими игроками, нужность этих запасных планов станет очевидной.

	In a best case scenario, of course, the players will find all three clues. There’s nothing wrong with that. They can use those clues to confirm their suspicions and reinforce their conclusions (just like Sherlock Holmes).

**В самом лучшем варианте** развития событий, конечно, игроки найдут все три улики. И в этом не будет проблемы. Они просто используют эти дополнительные улики для подтверждения своего мнения и умозаключений *(в точности как Шерлок Холмс)*.

	In a worst case scenario, they should be able to use at least one of these clues to reach the right conclusion and keep the adventure moving.

**В худшем случае**, они должны быть в состоянии использовать хотя бы одну из этих улик для производства верного вывода и продолжения движения по сюжету.

	And here’s an important tip: There are no exceptions to the Three Clue Rule.

**И вот вам важный совет:** Никогда не пренебрегайте Правилом Трёх Улик.

	“But Justin!” I hear you say. “This clue is really obvious. There is no way the players won’t figure it out.”

*«Но Джастин!»* – скажете вы – *«Такое количество улик слишком очевидно! Ведь у игроков не останется возможности их не понять!»*

	In my experience, you’re probably wrong. For one thing, you’re the one designing the scenario. You already know what the solution to the mystery is. This makes it very difficult for you to objectively judge whether something is obvious or not.

Однако, мой опыт говорит мне что скорее всего вы ошибаетесь. На минуточку, вы – автор этого сценария. *Вы уже знаете разгадку загадки.* И это делает для вас очень сложным объективную оценку того что очевидно, а что нет.

	And even if you’re right, so what? Having extra clues isn’t going to cause any problems. Why not be safe rather than sorry?

*Но даже если вы и правы, что тогда?* Наличие дополнительных улик не создаёт каких-то особенных сложностей. 
*Так бы и не подстраховаться?*

## Расширение для Правила Трёх Улик

![DeusEx](hhttp://cdn3.artofthetitle.com/assets/resized/sm/upload/w0/1s/eg/9w/deus_ex_contact-0-1080-0-0.jpg?k=d064f6e7f7)

	If you think about it in a broader sense, the Three Clue Rule is actually a good idea to keep in mind when you’re designing any scenario.

Если вы обдумаете всё это в более широком смысле, то Правило Трёх Улик представится вам очень хорошей идеей, которую стоит держать в голове при создании любого игрового сценария.

	Richard Garriott, the designer of the Ultima computer games and Tabula Rasa, once said that his job as a game designer was to make sure that at least one solution to a problem was possible without preventing the player from finding other solutions on their own. For example, if you find a locked door in an Ultima game then there will be a key for that door somewhere. But you could also hack your way through it; or pick the lock; or pull a cannon up to it and blow it away.

Ричард Гэриотт, один из создателей компьютерных игр [«Ultima»](https://ru.wikipedia.org/wiki/Ultima) и [«Tabula Rasa»](https://ru.wikipedia.org/wiki/Tabula_Rasa_(игра)), однажды сказал что работа игродела состоит в том чтобы предусмотреть для каждой внутриигровой проблемы как минимум одно готовое решение, предотвращающее самостоятельный поиск игроком её решения.

**Для примера**, если вы нашлм запертую дверь в *«Ultima»*, то наверняка где-то неподалёку есть от неё ключ. Однако, вместо того чтобы её открыть, вы также можете прорубиться сквозь неё, или вскрыть замок отмычкой, или поставить артиллерийское орудие напротив и вынести её напрочь.

	Warren Spector, who started working with Garriott on Ultima VI, would later go on to design Deus Ex. 
	
	He follows the same design philosophy and speaks glowingly of the thrill he would get watching someone play his game and thinking, “Wait… is that going to work?”

Уоррен Спектор, который в своё время начинал работать вместе с Гэрриоттом над «Ultima VI», позже создал свой собственный проект [«Deus Ex»](https://ru.wikipedia.org/wiki/Deus_Ex) следует той же философии и упоённо рассказывает о том, как наблюдая за кем-нибудь, играющим в его игру, ловил себя на восхищенной мысли: *«Погодите-ка... Это и правда сработает?»*.

	When designing an adventure, I actually try to take this design philosophy one step further: For any given problem, I make sure there’s at least one solution and remain completely open to any solutions the players might come up with on their own.

##### Создавая приключение, я стараюсь придерживаться той же философии, но делаю ещё один шаг вперёд: 

*Я должен быть уверен что для каждой проблемы в моей игре есть как минимум одно готовое решение и один вариант полностью открытого развития событий, для того чтобы игроки могли придумать своё собственно решение самостоятельно.*

	But, for any chokepoint problem, I make sure there’s at least three solutions.
		By a chokepoint, I mean any problem that must be solved in order for the adventure to continue.
	
Ну и, для устранения точки отказа, я обычно предусматриваю как минимум три готовых решения.
Под точкой отказа, здесь, я подразумеваю любую проблему, которая должна быть решена чтобы приключение продолжалось.

	For example, let’s say that there’s a secret door behind which is hidden some random but ultimately unimportant treasure. Finding the secret door is a problem, but it’s not a chokepoint, so I only need to come up with one solution. In D&D this solution is easy because it’s built right into the rules: The secret door can be found with a successful Search check.

Для примера, скажем, есть секретная дверь, за которой скрывается какое-нибудь случайное, совершенно неважное сокровище. Обнаружение этой секретной двери будет проблемой, но не станет точкой отказа, так что будет достаточно одного очевидного решения задачи.  
В случае «D&D» это решение будет очень простым, так как его содержат сами правила игры: *Секретная дверь может быть найдена при помощи броска Обнаружения.*

	But let’s say that, instead of some random treasure, there is something of absolutely vital importance behind that door. For the adventure to work, the PCs must find that secret door.

Однако, представим что вместо случайного сокровища за дверью находится нечто жизненно-важное. *И, чтобы сюжет приключения работал, персонажи просто обязаны найти эту дверь.*

	The secret door is now a chokepoint problem and so I’ll try to make sure that there are at least three solutions. The first solution remains the same: A successful Search check. To this we could add a note in a different location where a cultist is instructed to “hide the artifact behind the statue of Ra” (where the secret door is); a badly damaged journal written by the designer of the complex which refers to the door; a second secret door leading to the same location (this counts as a separate solution because it immediately introduces the possibility of a second Search check); a probable scenario in which the main villain will attempt to flee through the secret door; the ability to interrogate captured cultists; and so forth.

Теперь секретная дверь становится точкой отказа и мне нужна уверенность что на этот случай есть как минимум три решения.

*Первое решение будет точно таким же – успешным броском на Обнаружение.*

##### К этому мы должны прибавить: 

- Записку в каком-нибудь другом помещении c инструкцией для культиста «Спрятать артефакт за статуей Ра!» *(где, собственно, и находится наша секретная дверь)*;
- Журнал архитектора этого подземного комплекса с пометками о двери, находящийся в очень плохом состоянии;
- Вторую секретную дверь, через которую модно попасть в то же помещение *(это считается отдельным вариантом, так как провоцирует отдельный бросок на Обнаружение)*
- Опциональный кусок сценария, в котором главный злодей скрывается через эту дверь;
- Возможность узнать информацию через допрос пойманного культиста;
- *И так далее...*

		Once you identify a chokepoint like this, it actually becomes quite trivial to start adding solutions like this.

В общем, когда вы обнаруживаете подобную точку отказа, довольно несложно становится начать придумывать для неё решения.

		I’ve seen some GMs argue that this makes things “too easy”. But the reality is that alternative solutions like this tend to make the scenario more interesting, not less interesting. Look at our secret door, for example: Before we started adding alternative solutions, it was just a dice roll. Now it’s designed by a specific person; used by cultists; and potentially exploited as a get-away.

Я видел как некоторые ведущие утверждали что такой подход делает всё *«слишком простым»*. Но, в реальности, такие альтернативные возможности делают сценарии более интересными, а не менее интересными.

##### Возьмём ту же секретную дверь:   

- До начала создания альтернативных возможностей она была всего лишь броском на проверку навыка.
- Теперь же она приобрела индивидуальность, используется культистами и является потенциальным путём для побега.

		As you begin layering these Three Clue Rule techniques, you’ll find that your scenarios become even more robust. For example, let’s take a murder mystery in which the killer is a werewolf who seeks out his ex-lovers. We come up with three possible ways to identify the killer:

Как только вы начнёте использовать техники основанные на Правиле Трёх Улик, вы найдёте что ваши сценарии стали куда как прочнее *(и интересней – прим. пер.)*. 

### История оборотня

Для примера, возьмём ту же загадочную историю завязанную на убийцу-обротня убивающего своих бывших пассий. 

##### У нас есть три возможных пути для идентификации убийцы:

		(1) Patrol the streets of the small town on the night of the full moon.
		(2) Identify the victims as all being former lovers of the same man.
		(3) Go to the local butcher shop where the killer works and find his confessions of nightmare and sin written in blood on the walls of the back room.

1. Патрулировать улицы городка в полнолуние.
2. Обнаружить что все убитые были любовниками одного и того же человека.
3. Отправится в лавку мясника, место работы убийцы, и найти там его кошмарное признание в грехах, написанное кровью на стене задней комнаты.

		For each of these conclusions (he’s a werewolf; he’s a former lover; we should check out the butcher shop) we’ll need three clues.

Для каждого из трёх необходимых в итоге умозаключений *(убийца – оборотень, убийца – бывший любовник, нам надо пойти обыскать ту лавку)* необходимо наличие трёх улик.

		HE’S A WEREWOLF: Tracks that turn from wolf paw prints to human footprints. Over-sized claw marks on the victims. One of the victims owned a handgun loaded with silver bullets.

#### Убийца – оборотень!
- Отпечатки следов волка превращающихся в следы человека;
- Следы очень больших когтей на жертвах;
- У одной из жертв было оружие заряженное серебряными пулями.

#### Убийца – их бывший любовник!

		HE’S A FORMER LOVER: Love letters written by the same guy. A diary written by one victim describing how he cheated on her with another victim. Pictures of the same guy either on the victims or kept in their houses somewhere.

- Любовные письма от одного и того же адресата;
- Дневник одной из жертв с описанием измены любовника с другой жертвой;
- Изображения одного и того же человека найденные в вещах жертв или в их домах.

		CHECK OUT THE BUTCHER SHOP: A broken crate reading DANNER’S MEATS at one of the crime scenes. A note saying “meet me at the butcher shop” crumpled up and thrown in a wastepaper basket. A jotted entry saying “meet P at butcher shop” in the day planner of one of the victims.

#### Надо проверить магазин мясника!
- Разбитый ящик с надписью *«Мясо Дэннера»* найденный на одном из мест преступления;
- Записка *«Встретимся в лавке мясника»* выброшенная жертвой в мусорную корзину;
- Краткая заметка *«Встрет. с П. а мяс. лавк.»* в ежедневнике жертвы.

		And just like that you’ve created a scenario with nine different paths to success. And if you keep your mind open to the idea of “more clues are always better” as you’re designing the adventure, you’ll find even more opportunities. For example, how trivial would it be to drop a reference to the butcher shop into one of those love letters? Or to fill that diary with half-mad charcoal sketches of wolves?

Вы точно также можете создать свой сценарий с девятью различными путями к успеху. И, если вы будете открыты для концепции *«улик много не бывает»* во время создания сюжета приключения, вы обнаружите немало интересных перспектив.

##### Например:
- Насколько банальным будет оставить отсылку к мясной лавке в одном из тех любовных посланий?
- Или, как вариант, заполнить поля дневника полубезумными зарисовками волчьей фигуры?

		The fun part of all this is, once you’ve given yourself permission to include lots of clues, you’ve given yourself the opportunity to include some really esoteric and subtle clues. If the players figure them out, then they’ll feel pretty awesome for having done so. If they don’t notice them or don’t understand them, that’s OK, too: You’ve got plenty of other clues for them to pursue (and once they do solve the mystery, they’ll really enjoy looking back at those esoteric clues and understanding what they meant).

Немалое удовольствие от всего этого, раз уж вы дали себе разрешение на использование множества улик, вы можете получить привнеся в игру некоторое количество исключительно изящных, сложных для понимания и эзотерических знаков.

Если Ваши игроки cмогут распознать и понять их, то они буду чрезвычайно довольны собой.   
Если же нет, это тоже будет приемлемым, ведь у вас окажется запас других улик за которыми игроки смогут последовать *(и, единоразово разгадав загадку, смогут вернуться назад к нераспознаным уликам, чтобы понять что же они означают)*.

## Вывод #1: Поощрительное предоставление улик
![Friendly clues](http://1.bp.blogspot.com/-ujTvD-FC3Os/TaJajOEc0XI/AAAAAAAACX4/ZMHtU1XznPs/s1600/IMG_6943.JPG)

	The maxim “more clues are always better” is an important one. There is a natural impulse when designing a mystery, I think, to hold back information. 
	
Максима *«улик много не бывает»* крайне важна, так как естественным импульсом любого создателя сценария-загадки, как мне видится, является стремление к сокрытию информации. 

	This is logical inclination: After all, a mystery is essentially defined by a lack of information. And there’s a difference between having lots of clues and having the murderer write his home address in blood on the wall.

Это логично, ведь в конце концов любая «Тайна» характерна именно недостатком информации. 
И вот как раз здесь кроется разница между *«у нас много улик»* и *«убийца написал свой домашний адрес кровью на стене»*.

	But the desire to hold back information does more harm than good, I think. Whenever you hold back a piece of information, you are essentially closing off a path towards potential success. This goes back to Garriott’s advice: Unless there’s some reason why the door should be cannon-proof, the player should be rewarded for their clever thinking. 
	
Как бы-то ни было, соблазн сокрытия информации скорее вреден чем хорош. Когда бы вы ни захотели утаить кусок информации, вы всегда закрываете игрокам один из потенциальных путей к успеху. 

> Это возвращает нас к совету Гэрриотта: *Если нет причин для того чтобы дверь не выносилась из пушки, игрок должен быть вознаграждён за своё оригинальное мышление.*  

	Or, to put it another way: Just because you shouldn’t leave the key to a locked door laying on the floor in front of the door, it doesn’t mean that there shouldn’t be multiple ways to get past the locked door.

##### Или, иными словами:
*Раз уж вы не положили ключ у порога запертой им двери, то это означает возможность нескольких способов пройти через неё.*	

	With that in mind, you should consciously open yourself to permissive clue-finding. By this I mean that, if the players come up with a clever approach to their investigation, you should be open to the idea of giving them useful information as a result.

Держа это в голове, вы должны сознательно открыть себя возможности поощрительного предоставления улик. 

Говоря это, я имею ввиду что, *когда игроки подходят к расследованию с умом, вы должны быть открыты для идеи предоставления им полезной информации* в качестве результата.

	Here’s another way of thinking about it: Don’t treat the list of clues you came up with during your prep time as a straitjacket. Instead, think of that prep work as your safety net.

##### Или вот – альтернативное объяснение:
*Не надо относиться к заранее созданному списку улик как к смирительной рубашке.* Вместо этого, рассматривайте его в качестве средства подстраховки.

	I used to get really attached to a particularly clever solution when I would design it. I would emotionally invest in the idea of my players discovering this clever solution that I had designed. As a result, I would tend to veto other potential solutions the players came up with — after all, if those other solutions worked they would never discover the clever solution I had come up with.

Ранее, я практиковал *навязчивое предоставление игрокам именно той особенной возможности решения что я первоначально задумал*. Я эмоционально привязывался к идее того что мои игроки обнаружат именно эту замечательную, заранее созданную возможность.  
*В результате, я был склонен отвергать те возможности которые предлагали мне сами игроки,* ведь, если бы они воспользовались своими вариантами, они бы ни когда не нашли придуманную мной особенную возможность.


	Over time, I’ve learned that it’s actually a lot more fun when the players surprise me. It’s the same reason I avoid fudging dice rolls to preserve whatever dramatic conceit I came up with. As a result, I now tend to think of my predesigned solution as a worst case scenario — the safety net that snaps into place when my players fail to come up with anything more interesting.

*А потом я научился получать удовольствие от сюрпризов* что могут создать для меня мои игроки.  
И это – та самая причина, из-за которой я не мухлюю с бросками, вне зависимости от того какой у меня там задуман драматический концепт.

В данный момент, я склонен считать мою первоначальную концепцию *«единственно-верного замечательного решения»* худшим игровым сценарием из возможных. Моя система подстраховки, которая срабатывает в тот момент когда игроки что-нибудь завалили – намного более интересный вариант. 

	In order to be open to permissive clue-finding you first have to understand the underlying situation. (Who is the werewolf? How did he kill this victim? Why did he kill them? When did he kill them?) Then embrace the unexpected ideas and approaches the PCs will have, and lean on the permissive side when deciding whether or not they can find a clue you had never thought about before.

Первым что вам понадобится в процессе организации поощрительного поиска улик является понимание *(структурно – прим. пер.)* нижележащей ситуации:

- Кто этот оборотень?
- Как именно он убивает жертвы?
- Зачем он их убивает?
- Когда он их убивает?

Теперь, просто вбирайте все неожиданные идеи и поползновения персонажей, настроившись на поощрительный лад, не заботясь о Правильности их выборов, дайте им возможность найти улики тем способом, о котором даже не думали.   

## Вывод #2: Проактивные улики

![Beat'em up!](http://bridgeburnergaming.files.wordpress.com/2013/11/the-wolf-among-us-screenshot-bigby-fight-1024x576.jpg)

	A.K.A. Bash Them On the Head With It.

*Также известные как «Вдарь Им По Голове Вот Этим»*

	Sometimes, despite your best efforts, the players will work themselves into a dead-end: They don’t know what the clues mean or they’re ignoring the clues or they’ve used the clues to reach an incorrect conclusion and are now heading in completely the wrong direction. (When I’m using the Three Clue Rule, I find this will most often happen when the PCs don’t realize that there’s actually a mystery that needs to be solved — not every mystery is as obvious as a dead body, after all.)

Иногда, презрев ваши наилучшие устремления, игроки сами себя заводят в тупик. Они не понимают значения улик, или они игнорируют улики, или они делают на основании улик скороспелые и некорректные выводы, которые и ведут их в совершенно неверном направлении. 

Даже при использовании Правила Трёх Улик это может произойти, если игроки не поймут в чём же кроется тайна, ведь далеко не каждая тайна настолько очевидна как мёртвое тело.

	This is when having a backup plan is useful. The problem in this scenario is that the PCs are being too passive — either because they don’t have the information they need or because they’re using the information in the wrong way. The solution, therefore, is to have something active happen to them.


И это – тот самый случай, когда нужен запасной план. Проблемой подобного сценария становится пассивность персонажей, потому что они не обладают нужной информацией или же потому что они идут по неверному пути. 

*Логичным решением, в таком случае, будет нечто  самостоятельно свалившееся им на голову.*

	Raymond Chandler’s advice for this kind of impasse was, “Have a guy with a gun walk through the door.”


> Совет Рэймонда Чандлера, касательно подобных тупиков звучит так: *«Пусть парень с пушкой войдёт в комнату»*.

	My typical fallback is in the same vein: The bad guy finds out they’re the ones investigating the crime and sends someone to kill them or bribe them.
	
> Моё собственное решение для того же случая – *«Злодей узнаёт о расследовании и посылает ребят устранить персонажей или подкупить их»*.

	Another good one is “somebody else dies”. Or, in a more general sense, “the next part of the bad guy’s plan happens”. This has the effect of proactively creating a new location or event for the PCs to interact with.

Ещё один неплохой рецепт – *«опять кто-нибудь умер»*.
Или, в более общем смысле, *«ещё одна часть негодяйского плана реализована»*.  
Само собой, это должно выражаться в появлении нового места действия или события с которым персонажи смогут взаимодействовать.

	The idea with all of these, of course, is not simply “have something happen”. You specifically want to have the event give them a new clue (or, better yet, multiple clues) that they can follow up on.

Основной идеей всего этого, само-собой, является не *«просто что-то случилось»*. Вы специально хотите создать событие для того чтобы дать игрокам новую улику *(или, что даже лучше - несколько улик)*, за которыми они смогут следовать.

	In a worst case scenario, though, you can design a final “Get Out of Jail Free” card that you can use to bring the scenario to a satisfactory close no matter how badly the PCs get bolloxed up. For example, in our werewolf mystery — if the PCs get completely lost — you could simply have the werewolf show up and try to kill them (because he thinks they’re “getting too close”). This is usually less than satisfactory, but at least it gets you out of a bad situation. It’s the final backup when all other backups have failed.

В наиболее тяжёлом случае, вы даже можете разыграть карту *«выход из тени»*, если вы хотите довести сценарий до конца, вне зависимости от того, насколько ваши игроки всё испоганили. 

##### Для примера, возьмём нашу «Историю оборотня»

Если персонажи целиком и полностью всё провалили, пусть оборотень просто придёт к ним и попытается убить их *(в связи с тем что «Вы подобрались слишком близко!»)*. Конечно, в этом не много удовольствия, но, по крайней мере, это выручит вас из тупиковой ситуации. 

*Этот запасной план всегда поможет вам когда все остальные уже провалились.*

## Вывод #3: Сомнительность обманок

![The Red Herring](http://bni-grimsby.co.uk/sites/default/files/image/RHG%20logo%20jpeg%20(Medium).jpeg)

	Red herrings are a classic element of the mystery genre: All the evidence points towards X, but its a red herring! The real murderer is Y!

Обманные приёмы *(the red herrings – прим. пер.)* являются классическим элементом сюжетов-загадок:

> *«Все улики ведут к X, но это – подстава! Настоящий убийца – Y!!»*

	When it comes to designing a scenario for an RPG, however, red herrings are overrated. I’m not going to go so far as to say that you should never use them, but I will go so far as to say that you should only use them with extreme caution.

Однако, при создании сценария для ролевой игры, польза от их применения становится весьма сомнительной. Я, конечно, не зайду так далеко чтобы говорить *«Никогда их не используйте!»*, но я зайду достаточно далеко чтобы сказать что *их нужно использовать чрезвычайно осторожно*. 

	There are two reasons for this:

#### Для этого есть целых две причины:

	First, getting the players to make the deductions they’re supposed to make is hard enough. Throwing in a red herring just makes it all the harder. More importantly, however, once the players have reached a conclusion they’ll tend to latch onto it. It can be extremely difficult to convince them to let it go and re-assess the evidence. (One of the ways to make a red herring work is to make sure that there will be an absolutely incontrovertible refutation of it: For example, the murders continue even after the PCs arrest a suspect. Unfortunately, what your concept of an “incontrovertible refutation” may hold just as much water as your concept of a “really obvious clue that cannot be missed.)

**Во первых**, заставить игроков шевелить мозгами и так-то непросто, а использование обманок делает этот процесс ещё сложнее.  
*Что важнее, сделав какое-нибудь умозаключение, игроки носятся с ним как с писаной торбой.* После чего становится адски сложно сподвигнуть их как-то иначе посмотреть на улики.

##### Абсолютно неопровержимая обманка

Один из классических способов сделать обманку заключается в том чтобы сфабриковать *«абсолютно бесспорное опровержение»* истинной подоплёки.   
  
> **Например:** Убийства продолжаются даже после ареста подозреваемого.

К несчастью, такая концепция *«абсолютного опровержения»* слишком разжижает концепцию *«действительно очевидной улики, которую невозможно не найти»*.

	Second, there’s really no need for you to make up a red herring: The players are almost certainly going to take care of it for you. If you fill your adventure with nothing but clues pointing conclusively and decisively at the real killer, I can virtually guarantee you that the players will become suspicious of at least three other people before they figure out who’s really behind it all. They will become very attached to these suspicions and begin weaving complicated theories explaining how the evidence they have fits the suspect they want.

**Во вторых:** на самом деле, у вас нет необходимости создавать обманки, ведь игроки склонны создавать их самостоятельно.  
Даже, если в вашем приключении нет ничего лишнего, а все улики окончательно и однозначно указывают на персону убийцы, я почти наверняка могу гарантировать вам, что игроки успеют заподозрить, по меньшей мере, трёх людей, прежде чем сообразят что к чему.

Они очень привяжутся к этим своим подозрениями и начнут строить убедительные комплексные теории, объясняющие то, как именно все имеющиеся улики подкрепляют их.

	In other words, the big trick in designing a mystery scenario is to try to avoid a car wreck. Throwing red herrings into the mix is like boozing the players before putting them behind the wheel of the car.

*Другими словами, самая большая уловка применимая при создании сценария-загадки – это просто постараться избежать катастрофы.*

Ведь выдавая игрокам кучу обманок вы, фактически, подливаете им спиртного, зная что скоро они сядут за руль.

## Вывод #4: БУДЬ ГОТОВ 
![Moar backups]()

	You’ve carefully laid out a scenario in which there are multiple paths to the solution with each step along each path supported by dozens of clues. You’ve even got a couple of proactive backup plans designed to get the PCs back on track if things should go awry.

Итак, вы выпестовали свой сценарий, со множеством путей решения загадки, в котором на каждый шаг расследования полагается по дюжине улик.

У вас имеется несколько проактивных запасных планов предназначенных для возвращения персонажей на путь истинный, и на случай если всё пойдёт не так. Впрочем, у вас ничего не может пойти «не так»!

	Nothing could possibly go wrong!
	… why do you even saying things like that?

*А почему вы собственно так думаете?*

	The truth is that you are either a mouse or a man and, sooner or later, your plans are going to go awry. When that happens, you’re going to want to be prepared for the possibility of spinning out new backup plans on the fly.

На самом деле, истина в том что, будь вы человеком или мышью, рано или поздно ваши планы идут коту под хвост. И когда это случается вам очень хочется быть готовым изящным движением извлечь из запазухи очередной запасной вариант. 

	Here’s a quote from an excellent essay by Ben Robbins:

##### Приведу цитату из замечательного эссе Бена Роббинса:

	Normal weapons can’t kill the zombies. MicroMan doesn’t trust Captain Fury. The lake monster is really Old Man Wiggins in a rubber mask.

> Обычное оружие не убивает зомби. Микромэн не верит Капитану Фьюри. Озёрное чудище и в самом  деле – Старик Виггинс в резиновой маске.  

	These are Revelations. They are things you want the players to find out so that they can make good choices or just understand what is going on in the game. Revelations advance the plot and make the game dramatically interesting. If the players don’t find them out (or don’t find them out at the right time) they can mess up your game.

> Всё это – Откровения. Всё это – вещи, которые ваши игроки должны найти чтобы принять верные решения или просто понять что же происходит в вашей игре. Откровения дополняют фабулу и делают её более драматичной. Если игроки не пересекутся с ними *(или не пересекутся с ними вовремя)*, это может испортить всю игру.

	I recommend this essay highly. It says pretty much everything I was planning to include in my discussion of this final corollary, so I’m not going to waste my time rephrasing something that’s already been written so well. Instead, I’ll satisfy myself by just quoting this piece of advice from it:

Я крайне рекомендую вам ![прочесть это эссе](http://pnprpg.ru/blog/2014/08/25/arsludi-revelations/).   В нём замечательно рассказывается о многом из того что я хотел бы написать в этом, финальном, выводе, однако я не хочу зря тратить время на пересказ того что и так уже написано превосходно.

##### Вместо этого, я приведу ещё одну цитату:

	Write Your Revelations: Writing out your revelations ahead of time shows you how the game is going to flow. Once play starts things can get a little hectic – you may accidentally have the evil mastermind show up and deliver his ultimatum and stomp off again without remembering to drop that one key hint that leads the heroes to his base. If you’re lucky you recognize the omission and can backtrack. If you’re unlucky you don’t notice it at all, and you spend the rest of the game wondering why the players have such a different idea of what is going on than you do.

> **Запишите ваши Откровения:**  
> Запишите ваши Откровения загодя, до начала вашей игры.  
> В горячке начала игры вы можете что-нибудь упустить: Например, показав вашего ГлавЗлодея, который предъявляет свой Ультиматум и топает восвояси, вы можете забыть обронить ключ, что приведёт героев к его убежищу.   
> Если повезёт, вы заметите и исправите недочёт сразу. Если же нет, вы ничего не заметите и потратите какое-то время, удивляясь почему игроки никак не додумаются до того что же им нужно *(по вашему)*, делать.

	As we’ve discussed, one way to avoid this type of problem is to avoid having “one key hint” on which the adventure hinges. But the advice of “writing out your revelations ahead of time” is an excellent one. As Robbins says, this “should be a checklist or a trigger, not the whole explanation”.

Как мы уже выяснили, единственный способ устранить проблемы этого типа – устранить улики одиночного типа для центральной сюжетной линии. 

Однако, сам совет *«Запишите ваши Откровения до игры»* – великолепен. Как говорит сам Роббинс *«тут не нужно развёрнутое объяснение, достаточно простого списка или описания триггера»*. 

	What I recommend is listing each conclusion you want the players to reach. Under each conclusion, list every clue that might lead them to that conclusion. (This can also serve as a good design checklist to make sure you’ve got enough clues supporting every conclusion.) As the PCs receive the clues, check them off. (This lets you see, at a glance, if there are areas where the PCs are missing too many clues.)

Я лично рекомендую вам записать каждое умозаключение, которое, по вашему, должны сделать игроки. Под каждым умозаключением также перечислите улики которые к этим умозаключениям приводят. 

*Этот список также послужит хорошим «тестом на прочность» для вашего сюжета, позволяющим убедиться что у вас предусмотрено достаточно улик для каждого умозаключения.*

Как только персонажи получат ту или иную улику, вычеркните её.  
*Это позволит вам в любой момент увидеть какие именно улики недополучили герои.* 

	Finally, listen carefully to what the players are saying to each other. When they’ve actually reached a particular conclusion, you can check the whole conclusion off your list. (Be careful not to check it off as soon as they consider it as a possibility. Only check it off once they’ve actually concluded that it’s true.)

Наконец, внимательно прислушивайтесь к тому что игроки говорят друг другу. Когда они производят действительно верное умозаключение, вычеркните это умозаключение из своего списка.  
*Будьте осторожны! Не вычёркивайте его пока они рассматривают его только как возможность, но не убедились в том что их предположение – правда.*

	If you see that too many clues for a conclusion are being missed, or that all the clues have been found but the players still haven’t figured it out, then you’ll know it’s probably time to start thinking about new clues that can be worked into the adventure.

Если же вы видите что слишком много улик осталось за бортом, или они все найдены, но игроки никак не возьмут в толк что же это всё означает, то наступило время подумать о новых уликах, которые вернут ваше приключение в рабочее состояние.

## Напоследок

	Basically, what all of this boils down to is simple: Plan multiple paths to success. Encourage player ingenuity. Give yourself a failsafe.

	And remember the Three Clue Rule:

	For any conclusion you want the PCs to make, include at least three clues.
	
##### В принципе, всё вышеописанное сократить до:
- Запланируйте несколько путей к успеху;
- Поощряйте в игроках  изобретательность;
- Предусматривайте подстраховку.

##### И помните о Правиле Трёх Улик:
*Для каждого вывода на который вы хотите навести, предусмотрите, самое малое, три улики.*

*[©The Alexandrian, 08.05.2008](http://thealexandrian.net/wordpress/1118/roleplaying-games/three-clue-rule)*   