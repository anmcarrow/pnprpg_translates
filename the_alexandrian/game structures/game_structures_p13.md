**From:**
http://thealexandrian.net/wordpress/15222/roleplaying-games/game-structures-part-13-custom-structures

**Title:** Структура игры, ч.13: Самодельные игровые структуры (перевод)

![The Alexandrian splash](http://pnprpg.ru/wp-content/uploads/2014/08/the_alexandrian_splashes.png)

С оригиналом статьи Вы можете ознакомиться [здесь](http://thealexandrian.net/wordpress/15222/roleplaying-games/game-structures-part-13-custom-structures). 

When I designed The Lost Hunt for Fantasy Flight Games, I launched the scenario by having an elven village attacked by a kehtal (a servitor of the demon gods of Keht). The idea was pretty simple: The PCs could then follow the trail of this murderous creature, which would lead them to the interdimensional rift in which the demon gods were imprisoned.nj

Когда я писал «The Lost Hunt RPG» для [«Fantasy Flight Games»](www.fantasyflightgames.com), я начал со сценария, в котором эльфийская деревня подвергается нападению кехталя (слуги могущественных демонических богов Кехта).

**Идея была проста:** Персонажи игроков могут просто идти  по следу этого смертоносного существа, который приведёт их к межпространственной трещине, из которой как раз готовятся освободиться заточённые там демоны.

<!--more-->

---

The tricky part was the actual tracking. Although I wasn’t thinking in terms of game structures back in 2001, I knew that this section of the adventure needed more weight to it than a simple skill check. The experience of the adventure couldn’t be, “Fight a monster, make a Wilderness Lore check, and – ta-da! – you’ve found the interdimensional prison of an ancient god cult!”

#####Вся хитрость была в самом процессе выслеживания
Несмотря на то, что тогда, в 2001, я ещё не задумывался над всеми этим в терминах игровой структуры, я уже тогда осознавал что эта часть приключения должна быть чем-то большим, чем простая проверка навыков персонажей. 

Игровой опыт этого приключения не должен был сводиться к  «Побей монстра, прокинь проверку «Выживания в глуши» и оп-ля! Ты обнаружил межпространственную тюрьму древнего демонического культа!»

So what I ended up doing was crafting a custom game structure for tracking: Following the trail required five successful Wilderness Lore checks (DC 20). Each failure would force the PCs to backtrack (requiring an additional success in order to find the trail they lost). Each successful check would bring them to a “pit stop” along the trail, which was described in boxed text: One established the creature’s prodigious leaping ability; another brought them to another scene of carnage wrought by the creature; and so forth.

##### Поэтому я закончил свою мысль на том что создал самодельную игровую структуру-счётчик:
Пять успешных проверок на «Выживания в глуши» подряд позволяли успешно выследить монстра.  
Каждая неудачная попытка возвращала игроков на шаг назад (и требовала дополнительных успехов для нахождения потерянного следа).  
Каждая успешно пройденная проверка приводила персонажей к очередному «ключевому моменту» преследования, который был описан в соответствующем примечании-врезке:

- Один из таких моментов демонстрировал чрезвычайную прыгучесть монстра;
- Другой – его кровожадность, проявившуюся в очередной резне, и т.д. и т.п.

Nothing too complex here: I was basically adapting the concept of complex skill checks (as found in numerous RPG systems) and tweaking it a bit. But it did take a little bit of thought and a little bit of experimentation to nail down the details. Once I had tucked this custom “pit stop and backtrack” game structure into my mental toolkit, though, it proved useful time and time again: I’ve used it probably a dozen times since then.

В общем-то, это было не сложно: Я попросту адаптировал *(встречающийся во множестве других игр)* принцип комплексных проверок навыков и немного подогнал его под приключение.  
Помимо того, я потратил немного времени на раздумья и провёл пару экспериментов, чтобы закрепить все детали.

Единожды заимев такую структуру «ключевых моментов и возвратов» в своём мысленном инструментарии, я возвращался к ней снова и снова. Пожалуй, с тех пор я использовал её более дюжины раз.

This is, obviously, a very simple example of how you can create custom game structures to organize your prep and affect your players’ experience with the game world. In fact, it’s so straight-forward some of you are probably saying, “Duh.”

Как видите, это весьма простой пример того, как Вы можете создать собственную игровую структуру для реализации ваших идей и нового внутри-игрового опыта ваших игроков. 

Фактически, это настолько просто, что некоторые из вас захотят сказать – «Подумаешь!»

So let’s tackle something a little more complicated.

Что ж, тогда давайте создадим кое-что посложнее.

BETWEEN THE STARS

##Блуждая меж звёзд

![sample astronomy picture](http://www.thealexandrian.net/images/20120430b.jpg)

Campaign Concept: The PCs are the crew (and possibly owners) of a starship plying the interstellar trade routes. Although some planet-side activity might croup up, the focus of this campaign is going to be on the voyages of the ship itself.

###Концепция кампании
Главные герои – команда (а возможно – и владельцы) космического корабля, курсирующего по межзвездным торговым путям. Несмотря на некоторую игровую активность на планетах, сама игровая кампания фокусируется непосредственно на космических перелётах.

Macro-Structure: For the macro-structure of the campaign, I’m going to use Traveller. As discussed in Part 10, Traveller has a well-developed system for handling interstellar travel and trade. This system empowers the PCs to make decisions about where they’re going; what they’re trading; and so forth.

###Макро-структура

Для создания макро-структуры кампании я буду использовать ["Traveller"](http://ru.rpg.wikia.com/wiki/Traveller).  
Как я уже упоминал [в 10-ой части] (http://pnprpg.ru/blog/2014/09/25/thealexandrian-game-structures-p10/), «Traveller» обладает отличной системой для межзвездных путешествий и торговли. 
Он дает игрокам возможность принимать решения о том куда путь держать, чем торговать и так далее. 

Scenario Triggers: As we also discussed in Part 10, however, this game structure is incomplete. It has a closed resolution loop (go to starport, deliver goods, pick up goods, go to starport), but it lacks vertical integration. So the first thing we need to figure out is the trigger we’re going to use for transitioning from the trade-and-travel macrostructure to the scenarios that will probably fill most of our actual playing time.

###Сюжетные сигнальные элементы

Тем не менее, как я уже писал в 10-ой части цикла, *игровая структура «Traveller» неплоноценна*. 

В ней присутствует собственной кольцо разрешений заявок *(добраться до космопорта > забрать и доставить груз в точку назначения > добраться до космопорта, и т.д.)*, однако в ней же отсутствует вертикальная интеграция вложенных структур.

Таким образом, перво-наперво нам необходимо понять – какое действие или событие станет сигнальным элементом запускающим переход от макро-структуры путешествий и торговли к тем более конкретным сюжетам что, скорее всего, будут занимать большую часть реального игрового времени.

I’m going to propose that, just like a dungeon has rooms and a wilderness has hexes, this campaign has voyages. In other words, just like we fill a room or a hex with content, we’re going to fill each trip from one star system to another with content. (Of course, some dungeon rooms are empty and some of our voyages may be uneventful. We’ll come back to that later.)

Таким образом я могу предложить, так же как подземелье на комнаты, а дикие земли на шестиугольники, разбить события этой игровой кампании на отдельные рейсы.

Иными словами, мы можем наполнить содержимым любой рейс между звёзд тем же способом которым мы наполняем содержимым комнату в подземелье или шестиугольник на карте. 

> Конечно же некоторые комнаты могут пустовать, а некоторые рейсы могут быть скучны и лишены происшествий. Но об этом чуть позже.

BETWEEN THE STARS – KEYING VOYAGES

## Блуждая меж звёзд - легенда для путешествия
![sample spaceship](http://futuredude.com//wp-content/uploads/2012/04/prometheus-film-alien-prequel-blueprints.jpg)

We all know how to key a dungeon room or a hex: You write a number on the map and then you use that number to reference a description of the content of the room or hex. How do we key voyages? In other words, when the PCs leave a starport how do we know what this voyage will contain?

Мы все знаем как однозначно прописать информацию о комнате подземелья или о шестиугольнике на карте: вы помечаете комнату или шестиугольник каким-нибудь номером и привязываете к этому номеру описание *(легенду)* содержимого, вынесенное за пределы карты. 

*Однако, как нам прописать содержимое конкретного рейса?*

Иными словами, как мы можем узнать что именно поджидает героев в предстоящем перелете уже в тот момент отбытия из космопорта?

Linear Sequence: A simple solution would be a linear sequence. You prep a scenario for their first voyage (no matter where they’re headed); then you prep a scenario for their second voyage; and so forth. The obvious disadvantage of this approach is that it doesn’t include meaningful choice for the PCs.

###Линейная последовательность

*Самым простым решением будет линейная логическая последовательности событий.*

- Вы просто готовите какой-нибудь сюжет для их первого рейса, *(тут даже не важен первоначальный пункт отправления)*;
- А затем, принимая во внимание их предыдущие действия, готовите второе, третье, и так далее.

Основным недостатком такого подхода будет малая значимость решений (выборов) и воли персонажей. *(Они просто идут туда, куда захочет ведущий. И это печально. - прим. пер.)*

Random: We could prep several voyages and then randomly determine which one we’ll use on any given trip. But, of course, once again we’ve eliminated meaningful choice for the PCs.

### По воле случая
Ещё мы можем приготовить несколько возможных вариантов развития событий и затем, случайным образом, определить что же все-таки произойдет в том или ином рейсе.
*Однако это также лишает персонажей возможности выбора.*

Space Hexes: We could key each hex on the subsector map with content. Couple of problems, though: First, any given voyage will actually contain multiple hexes. Second, because the campaign isn’t exploratory in nature there will be a lot of hexes they’re unlikely to visit (since they’ll probably be sticking to direct routes between planets). We could, of course, limit our prep to hexes near established trade routes and then implement a system for randomly determining which hex’s content on the flightpath gets triggered for any particular voyage. But doing that actually suggests what might be an easier approach…

###Космические шестиугольники

По аналогии с Hexcrawl мы можем разделить космос на зоны с тем или иным содержимым.

##### Однако и здесь мы сталкиваемся с некоторыми проблемами:
* Во-первых, всего один рейс может включать в себя сразу несколько зон;
* Во-вторых, так как сутью игровой кампании не является исследование пространства, то персонажи, скорее всего, пропустят или же просто не станут посещать некоторые из зон и будут придерживаться наиболее прямых и известных маршрутов;

> Вы конечно можете ограничиться заполнением только зон лежащих вблизи торговых путей, а затем случайным образом определять какая из посещаемых зон «среагирует» на появление путешественников. *Но что-то мне подсказывает, что всё можно сделать гораздо проще...*

Routes: What if we just key each route with content? When the PCs take a particular trade route, we trigger whatever content we keyed to that route. A potential problem here would be PCs who settle down into servicing a regular route: Once they’ve used up the keyed content for the route, there’ll be nothing new to experience the next time they take it. We could mitigate this by randomly determining cargo destinations (so that the PCs would be less likely to settle into a regular pattern) or by keying multiple scenarios to a single route (this would increase the prep load, but make it harder to completely “burn out” a given route).

###Прокладка маршрутов

*А может просто прописывать содержимое каждого маршрута?*

В таком случае, когда герои выбирают тот или иной путь, мы сразу сможем применить к ним его прописанное содержимое. 

Потенциальная проблема здесь в том, что игрокам может понравиться перемещаться только по одному маршруту: Рано или поздно они исчерпают ресурс этого маршрута и не встретят на нем уже ничего нового *(Что в общем-то логично и игроки сами должны это понимать - прим. пер.)*.

Эффект исчерпания ресурсов маршрута можно смягчить за счет случайного назначения пунктов выгрузки товара *(что будет мешать персонажам пользоваться только излюбленным маршрутом)*, или включив в один маршрут сразу несколько комплектов содержимого.  

Это сильно усложнит подготовку для ведущего, но так у игроков будет меньше шансов исчерпать события того или иного маршрута. 

BETWEEN THE STARS – SPICING THE STRUCTURE

##Блуждая меж звёзд: приправы и дополнения

So our basic structure looks like this: We key each trade route with an encounter or scenario which is experienced when the PCs take the route. In addition, we randomize cargo destinations to discourage the PCs from wearing a groove into a particular trade route.

##### Итак, наша структура выглядит следующим образом: 
- Для каждого маршрута прописаны события, с которыми персонажи могут столкнуться;
- В дополнение, случайное определение пунктов назначения предупреждает возможность игроков привыкнуть к одному маршруту.

That, by itself, would give us enough structure to run a campaign: We could draw up the local subsector, map out the trade routes, key them, and start play. But what could we do to spice things up – adding flavor, complexity, and/or detail to the campaign?

*Такой структуры, самой по себе, уже достаточно для запуска нашей кампании.* 

Создаем начальный субсектор пространства, намечаем карту основных торговых путей, придумываем для них сюжеты и ключи, *3, 2, 1, поехали!*

Однако как мы можем сделать эту кампанию более вкусной, полной ярких деталей и/или запоминающихся моментов? 

Random Chance: Instead of a route’s encounter happening automatically when the route is taken, we could have a randomized scenario check. Since the players won’t know whether there will be complications on a particular voyage, this will make the campaign less predictable (and also possibly less frustrating). Setting the right probability of experiencing a route scenario will probably require some experimentation: Will the PCs end up taking multiple routes on most journeys (getting from planet A to planet C via planet B)? How interested are the players in the actual trade mechanics of the game (as opposed to using the trade mechanics as a mere method of delivering content)? And so forth.

###Один шанс из...

Что если вместо автоматического срабатывания сигнальных элементов маршрута мы введём дополнительную *«проверку на срабатывание»*?

В таком случае, игроки не будут наверняка знать с чем столкнутся, да и встретят ли что-то вообще в течение рейса. Это, в свою очередь сделает кампанию менее предсказуемой и снизит шанс возможных разочарований *(так как уменьшит скучную предсказуемость – прим. ред.)*.

##### Выбор подходящей степени вероятности срабатывания может потребовать ряда экспериментов (и наблюдений):

- Станут ли герои, в результате, выбирать разные маршруты для своих путешествий?
- Захотят ли лететь с планеты «A» на планету «B» через планету «C»?
- Насколько игроки будут заинтересованы в использовании механики торговли, в противовес механике путешествий или простой доставки груза? 
- И так далее.

For the sake of argument, let’s say that we want roughly a 1 in 3 chance of triggering a scenario. (A roll of 1-2 on 1d6.)

Например, давайте скажем что мы хотим срабатывания нашего сигнального элемента маршрута примерно в одной трети случаев *(1 или 2 на d6)*.

Scenario Sources: Now that we’ve randomized the occurrence of scenarios, we can use that same mechanic to include encounters from non-route-based sources.

###Источники сценариев

Итак, теперь мы сделали случайным проявление привязанных к маршруту сценарных элементов и можем использовать эту же механику для активации событий не связанных напрямую с действиями персонажей.

First, we’re going to seed our cargo and passenger tables with scenario triggers. For example, carrying a shipment of positronic brains makes it more likely to be targeted by rogue robotic hijackers. Or a particular passenger might be targeted for assassination.

##### Теперь, во первых:
Мы заведём отдельные таблицы событий для грузовых и пассажирских рейсов, с собственным набором сценарных элементов в каждой.

> Например, при перевозке груза искусственных позитронных мозгов, вы имеете немалые шансы подвергнуться нападению пиратской банды роботов, а при перевозке VIP-персоны столкнуться с наёмным убийцей.

Second, our scenario check (which is performed once per route) is now revised: On a roll of 1 we trigger a route scenario; on a roll of 2-3 we trigger a passenger scenario; on a roll of 4-5 we trigger a cargo scenario. A roll of 6 indicates no encounter.

##### И во вторых 
Нам стоит пересмотреть и дополнить наши «проверки на срабатывание сценарного элемента» *(которые мы делаем один раз за рейс)*:

- Теперь при выпадении единицы мы запустим сценарий маршрута;
- При 2 или 3 – «пассажирский сценарий»;
- При выпадении 4 или 5 начнём сценарий связанный c грузом; 
- При выпадении шестерки ничего не будем делать и рейс завершится без осложнений.

Scenarios are theoretically being triggered on rolls of 1-5 on 1d6, but our practical odds of experiencing a scenario on any given route will remain roughly 1 in 3 because the PCs may not be carrying cargo or passengers with scenario triggers.

*В теории, тот или иной сценарий всегда будет активирован при значениях от 1 до 5 при броске d6.*

Однако на практике получается что вероятность напороться на один из сценариев составляет 1/3, так как персонажи не могут перевозить и груз и пассажиров одновременно. 

Weighted Route Tables: Instead of just keying a unique encounter (or a set of unique encounters) to each route, we could instead key each route with a weighted scenario table: So in the Black Expanse you’re more likely to get hit by pirates, while in the Inner Systems you’re more likely to get hit with a random audit.

###Приоритеты маршрутов
Вместо того что бы снабжать каждый маршрут собственным  событием *(или событиями)*, мы можем привязать к маршруту таблицу приоритетов событий:  

- Например, в пространстве Чёрного Простора у героев будет гораздо большая вероятность подвергнуться нападению пиратов;
– А во Внутренних Мирах их, напротив, могут неожиданно подвергнуть таможенному досмотру.

(Alternatively, we could rebuild our scenario check and include “region scenarios” as a fourth type: So each route would be keyed with a unique scenario; each region would have a random scenario table; and we’d also have cargo/passenger scenarios.)

##### Мы можем перестроить наши сценарные проверки и иначе, привязав часть из них к конкретным регионам: 

- Каждый маршрут имеет закреплённый за ним уникальный сценарий;
- Каждый регион космоса также имеет свою таблицу случайных событий;
- И, наконец, у нас всё ещё есть сценарии связанные с пассажирами/грузом.

“Empty” Voyages: As noted above, we’ve now created “empty” voyages (i.e., voyages on which no scenarios will be triggered). In order to spice these up, I’m going to take a page from Ars Magica, combine it with the character creation rules for Traveller, and create a game structure for handling “down time”: Improving your skills. Improving your ship. Working on research projects. And so forth.

###«Пустые» рейсы

Как уже отмечалось выше, в нашей структуре присутствуют «пустые» рейсы *(рейсы без осложнений)*. 

Чтобы сделать интересными и их тоже, я собираюсь взять кое-что из [Ars Magica][http://en.wikipedia.org/wiki/Ars_Magica], совместить это с  механикой создания персонажа  из «Traveller», и создать специальную игровую структуру для заполнения «пустующего времени».

##### Эта структура будет включать:
- Механику улучшения корабля;
- Механику прокачки навыков персонажа;
- Механику исследовательских (лабораторных) проектов;
- И т.д. и т.п.

Dockside Encounters: Another possibility would be adding structures for dockside encounters and/or scenarios. But I’m actually going to deliberately eschew this sort of thing: I want this campaign to be focused on the ship.

###Портовые происшествия

Другая возможность разнообразить игру – добавление в нашу структуру различных портовых происшествий. 

Я осознанно игнорирую эту возможность, так изначально развиваю концепцию игровой кампании вокруг космических перелётов.

While it’s certainly possible that the players will get tangled up in some planet-side intrigue, by specifically excluding this content from the campaign structure I’ll be steering the focus of the game away from it: Docking will generally be the boring bit that bridges the gap between the exciting stuff.

Конечно, даже так есть вероятность, что игроки ввяжутся в какую-нибудь интригу планетарного масштаба, однако исключив механизмы подобных интриг из игровой структуры я сместил с них и фокус внимания игроков.

> Время от посадки до взлёта как правило будет скучным промежутком между захватывающими событиями остальных участков игры.