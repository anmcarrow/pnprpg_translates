**From:** http://thealexandrian.net/wordpress/15164/roleplaying-games/game-structures-part-7-playing-with-hexcrawls

**Title:** Структура игры, ч.7: Играем с Hexcrawl 

![The Alexandrian splash](http://pnprpg.ru/wp-content/uploads/2014/08/the_alexandrian_splashes.png)

С оригиналом статьи Вы можете ознакомиться [здесь](http://thealexandrian.net/wordpress/15164/roleplaying-games/game-structures-part-7-playing-with-hexcrawls).

---

Once an experienced GM has learned how to use a particular game structure, it’s usually trivial for them to “bling it out” with additional game structures that add flavor, complexity, or detail to a scenario.

Когда опытный ведущий приобретает умение использовать ту или иную конкретную *игровую структуру*, ему несложно становится расцветить её, добавив красок, завершённости и деталей к сценариям на её основе.

If we take the basic structure of a hexcrawl, for example, what could we add (or tweak) to change (and hopefully improve) our game?

Итак, если мы возьмём [*структуру* Hexcrawl](http://pnprpg.ru/blog/2014/09/01/thealexandrian-game-structures-p6/), как думаете, что мы могли бы добавить к ней *(или поменять в ней)*, для того чтобы изменить *(а может и улучшить)* нашу игру?
<!--more-->

---

Random Encounters: A simple example. Just as random encounters add life and activity to a dungeon complex, they can also make a wilderness setting come alive. And it’s pretty easy to add periodic encounter checks to our hexcrawl procedures. Of course there are still questions to be answered about our exact methodology: Do we check once per hex? Once per day? Several times per day?

###Случайные встречи

Простейший пример: Точно также как [Случайные встречи](http://ru.rpg.wikia.com/wiki/Random_encounter) добавляют жизни и действия в ваше подземелье, они могут сделать ваши дикие просторы более живыми *(и убедительными – прим. ред.)*. 

*Вам всего лишь нужно добавить регулярную проверку на возможность случайной встречи в ваши стандартные Hexcrawl-процедуры.* 

##### Конечно, здесь есть несколько важных вопросов: 
- Насколько часто происходят проверки? 
- Один раз на шестиугольник? 
- Один раз в день? 
- Несколько раз в день?

Hexes Are Big: Does it make sense for PCs to automatically find a hex’s keyed encounter as soon as they enter the hex? Probably not. A typical 30-mile hex (like those used in the original Darlene map of Greyhawk) is larger than New York City and two-thirds the size of Rhode Island. That’s a lot of territory for a couple dozen orcs or a lonely cave entrance to get lost in.

### Размерность Шестиугольников

Стоит ли игрокам получать случайную встречу сразу, когда они входят в очередной шестиугольник? *Полагаю что нет.*

Стандартный шестиугольник размером в 30 миль (например, тот, который используется в *[«Darlene map of Greyhawk»](http://ghmaps.net/greyhawk-maps/)*) большe чем весь Нью-Йорк и равен двум третям штата Род-Айленд.  
*В нём точно могут попросту затеряться пара десятков орков или одинокий вход в  пещеру.*

To model this, we could make the chance of experiencing a hex’s keyed encounter variable. We could even vary the probability of this (making it less likely to encounter hidden locations and more likely to encounter highly visible locations).

Что бы смоделировать это, мы должны сознательно определить шанс встречи с прописанным в легенде шестиугольника событием. 

Мы можем даже подтасовать вероятность такой встречи *(скажем, сделав такую вероятность ниже для замаскированных объектов и выше для очень заметных)*.

Navigating the Wilderness: Once you’ve left roads and well-beaten trails behind you, it’s relatively easy to become lost in the wilderness (particularly if you’re not properly trained). 

So rather than just letting players determine precisely the direction they want to go, we could add a skill check to determine whether or not they become lost (and, if they do, determine their true direction of travel randomly). To spice things up, we could set the difficulty of this check based on the terrain type they’re currently traveling through. We could even have weather conditions modify this check (so that, for example, it would be more difficult to find your way on stormy, overcast nights than when the stars were visible).

### Ориентирование на местности

Когда вы оставляете проезжий тракт за спиной и покидаете проторённые пути, вы остаётесь один на один с дикими просторами, в которых совсем не сложно заблудиться *(особенно если вы впервые в подобной ситуации)*.

В связи с этим, ещё до того как игроки точно определят для себя желаемое направление, вам нужно произвести проверку их навыков и выяснить есть ли у них шанс заблудиться прямо сейчас *(и если да – отправить их в другую, абсолютно случайную сторону)*.

Для пущей остроты, мы можем проводить проверку с учётом типа местности по которой герои держат путь. Мы даже можем сделать эту проверку зависящий от погоды.

> Дождливая и беззвёздная ночь - не лучшее время что бы найти путь до цели.

Mode of Travel: Are the PCs traveling at a normal pace, racing at high speeds, covering their tracks, spending time foraging, or crisscrossing their own path in order to thoroughly explore the local area? 

Based on these decisions, we could vary the speed at which they travel; the difficulty of navigation; the odds of finding local points of interest; and so forth.

### Особенности путешествия

##### С какой скоростью двигаются персонажи? 
- В нормальном темпе?
- Спешат?
- Заметают следы?
- Тратят время на мелкие грабежи?
- Или даже бродят кругами, тщательно исследуя местность?

Исходя из ответа на эти вопросы, мы можем варьировать их скорость передвижения, сложность ориентирования на местности, шансы найти что-нибудь интересное, и так далее.

Other Game Structures: Tulan of the Isles, a lesser known product written by Raymond E. Feist and Stephen Abrams in 1981, includes a full game structure for prospecting gems. The Ready Ref Sheets from included a similar system for prospecting, detailing the amount of time it takes to prospect a hex, the percentile chances of finding a vein of precious metal, and a methodology for randomly determining the type of vein and its value.

### Дополнительные игровые структуры

[«Tulan of the Isles»](http://www.crydee.com/node/9352) –малоизвестное творение Рэймонда Фейста и Стивена Абрамса, написанное ими в 1981 году, включает в себя полноценную игровую структуру предназначенную для поиска драгоценных камней.

[«The Ready Ref Sheets» от Judges Guild](http://www.rpgnow.com/product/932/Ready-Ref-Sheets-1978?it=1) содержит схожую систему для геологоразведки, советы о количестве времени, предоставляемого на исследование шестиугольника, расчета вероятности найти месторождение драгоценных металлов, а также методы определения типа найденного месторождения и подсчёта его оценочной стоимости.

I offer this up not as something that every hexcrawl campaign requires, but rather as an example of how we often don’t think about the game structures that we use. 

If your players decided they wanted to go prospecting, how would you adjudicate that at the table? Would the method you use remain balanced over time if the players decided to make prospecting a major part of their characters? Could you make it as much as fun as dungeoncrawling? (If not, why not? Think about it.)

Я предполагаю, что всё это нужно далеко не в каждой Hexcrawl-кампании, но это хороший пример того, что зачастую мы даже не задумываемся о той игровой структуре, которую используем.

*Как вы будете принимать решения за игровым столом, если ваши игроки решили заняться геологическими изысканиями?* 

Как долго ваша игра сохранит баланс, после того как игроки сделают разведку ископаемых важной частью своих персонажей?

Сможете ли вы сделать такую игру не менее увлекательной чем «зачистка подземелий»?
*Если – «нет», то почему? Подумайте над этим.*

Consider, too, how the availability of game structures subconsciously shapes the way we play the game. Would you, as a GM, be more likely to design a scenario hook in which the PCs are hired by a dwarven king to prospect potential gold mines in the Frostbite Peaks if you had a fun little mechanic for prospecting to build a larger situation involving goblin reavers, icingdeath undead, and rogue frostmancers around? Would your cash-strapped players be more likely to spontaneously consider prospecting in the wilderness a viable alternative for cash if the rulebook included a chapter of rules for it?

##### Рассмотрим, также, то как доступность той или иной игровой структуры формирует сам способ того как мы играем. 

Стала бы вам *(как ведущему)* более симпатична мысль о создании сюжетного хода типа *«Персонажей нанимает Подгорный Король для разведки золотых жил в горной гряде Клыки Мороза»*, если бы у вас была простая и забавная механика для геолого-разведки, позволяющая быстро создать общую картину происходящего, включая набеги гоблинов, морозную нежить и одичалых магов-холодомантов?

Стали бы персонажи ваших игроков, при безденежье, с большим интересом рассматривать спонтанную геологоразведку в глуши как жизнеспособную альтернативу другим способам заработка, если бы в книге правил нашлась глава про это?

*To be continued...
