**From:** http://thealexandrian.net/wordpress/15234/roleplaying-games/game-structures-part-14-scenario-structure-for-between-the-stars

**Title:** Структура игры, ч.14: Игровые структуры для межзвёздных блужданий

![The Alexandrian splash](http://pnprpg.ru/wp-content/uploads/2014/08/the_alexandrian_splashes.png)

С оригиналом статьи Вы можете ознакомиться [здесь][http://thealexandrian.net/wordpress/15234/roleplaying-games/game-structures-part-14-scenario-structure-for-between-the-stars]

Now that we have a custom campaign structure for a “Between the Stars” campaign, let’s turn our focus to the scenarios that will be triggered by that campaign structure.

Теперь, когда у нас есть специальная структура для кампании в стиле «блуждая меж звёзд», давайте посмотрим на  сюжеты, которые инициируются этой структурой.

<!--more-->

---

With quite a few of these scenarios, of course, we can use scenario structures that we’re already familiar with: Someone is murdered on board; grab our scenario structure for mysteries. The ship stumbles across a wandering asteroid studded with alien architecture; whip out the maps for an old-fashioned dungeoncrawl. We might want to give some thought to the types of hooks we can use to initiate each scenario narratively, but there’ll be no need to reinvent the wheel in designing the scenarios themselves.

При наличии большого количества возможных сценариев мы, конечно, можем выбрать из них только те, которые используют уже знакомые нам игровые структуры.

#### Например
- Таинственное убийство во время перелёта? Берём и используем структуру сюжета-загадки.
- Астероид с древними нечеловеческими  руинами прямо по курсу? Настало время старой доброй *«зачистки подземелья»*.

в какой-о момент, мы можем задуматься о первоначальной сюжетной зацепке для каждого такого сценария. Однако нам не придётся изобретать велосипед для создания их самих.

(Random thought: If we’re looking for a semi-generic structure for constructing scenario hooks we could define them according to (a) when they’re triggered; (b) what officer will receive the trigger; and (c) what the hook is. For example, a scenario in which the PCs are approached to smuggle a cargo might be triggered in dock before the voyage starts; target the cargomaster; and take the form of a comm call asking for a meeting at a dockside bar. The “asteroid of alien architecture” might be hooked with the navigator making a Sensors check (on a success they detect the asteroid a fair distance away; on a failure they stumble too close to it and get caught in an automated tractor beam). A murder scenario might be hooked with the security officer making a Security check (success indicates they find the body on a routine sweep; failure indicates that a random passenger finds the body).

#### Внезапная мысль
В поисках более-менее общей структуры создания сюжетных зацепок, имеет смысл сразу выделять для каждой зацепки:

- а) В какой момент она инициируется;
- б) Кем именно она инициируются;
- с) Ну и что представляет собой сама зацепка;

##### Например, сценарий c контрабандой...
Сценарий c контрабандой может быть запущен *в порту* ещё *до появления в нём героев*.   
В такой ситуации, *возможный сигнальный элемент (и/или тот кто его инициирует) – работник грузового отсека*, ну а сама *зацепка – приглашение к встрече* в баре космопорта».

##### «Астероид с руинами» может быть обнаружен *при помощи проверки Cенсоров*: 
- В случае успеха проверки, путешественники успевают заметить астероид издалека; 
- При провале они внезапно натыкаются на него  и автоматика астероида захватывает их в силовое поле.

##### В сюжете с убийством зацепкой можно сделать проверку навыка Безопасность для офицера охраны: 
- Успех будет означать что именно он нашёл труп;
- Провал – что это сделал один из пассажиров.

For other scenarios, however, it may be useful to give some thought customizing new scenario structures. As an example of that, we’re going to look at the hypothetical example of hijacking scenarios. Before doing that, however, I want to make a couple of particular points.

Само-собой, для других сценариев этот метод так же может  послужить источником свежих мыслей по подгонке и оптимизации.

Для иллюстрации моего утверждения, давайте рассмотрим произвольны сюжет с ограблением. 
Однако, перед этим, я хочу подчеркнуть несколько важных моментов.

First, we’ll want to remember that the default goal of the campaign’s macro structure is to successfully run a ship in order to maximize profit. Which means that scenarios and hooks that either threaten the PCs’ profit or offer them opportunities for more profit will be most successful.

Во первых, напомню, что типовая цель любого макро-сюжета нашей кампании – это успешное, максимально прибыльное, завершение перелета. 
Это означает, что сюжеты/зацепки угрожающие прибыли или, наоборот, предлагающие возможность увеличения барышей будут наиболее успешны.

Second, I want to note that this entire section of the essay is entirely hypothetical: If this stuff were to be put to a proper playtest, I have little doubt that we’d discover that some of it doesn’t work in practice and other stuff could be greatly improved. That’s just part of the process.

Во-вторых, хочу отметить что вся эта часть моего эссе исключительно умозрительна. 

На практике может оказаться что часть предложенного просто не работает, а часть можно оформить и получше. *И это будет абсолютно нормальной частью процесса разработки.*

SCENARIO STRUCTURE: HIJACKING

##Сценарная структура: Захват

What we’re looking at here is any scenario where passengers, stowaways, or members of the crew attempt to take control of the ship.

Здесь рассмотрим сценарий включающий попытку пассажиров, безбилетников или членов экипажа взять контроль над судном в свои руки.

If the ship were relatively small in size, we could probably run this using location ‘crawl techniques. In other words, we could take a fully keyed map of the ship and then run the actions of the PCs and hijackers in “real time” so to speak. This would be appropriate for something like Air Force One.

Если корабль небольшого размера мы можем просто использовать структуру *«зачистки»*. 

Иными словами, нам нужно составить подробную карту корабля, на просторах которой будут действовать захватчики и их противники, так сказать, «в реальном времени». 

> Всё это, в таком случае будет походить на что-то вроде ["Самолета президента"][https://ru.wikipedia.org/wiki/Самолёт_президента].

But let’s assume that the ship is larger and that we want to create experiences that feel more like Die Hard or Under Siege.

Однако давайте представим что корабль большой, а мы хотим игровую структуру похожую на ["Крепкий орешек"][https://ru.wikipedia.org/wiki/Крепкий_орешек_(фильм,_1988)] или ["В осаде"][https://ru.wikipedia.org/wiki/В_осаде]

Node Map of the Ship: First, for purposes of navigation, let’s create a node map of the ship. This abstract representation of movement aboard ship will allow both PCs and hijackers to make meaningful decisions about where they’re going and how they’re going to get there without getting bogged down in tracking things corridor-by-corridor and room-by-room. (Although for certain key areas – like the bridge of the engine room – we may still want to draw-up detailed maps for tactical purposes.)

### Условная карта

##### Во-первых
Для навигации по кораблю, нам нужно создать его *«точечно прописанную»* карту *(мостик, отсек "А", отсек "Б", столовая, пропускаем, кочегарка, камбуз, например – прим. ред.)*. 

Такая абстрактная карта понадобится нам для координации передвижений персонажей и их противников, а также в качестве основы для их значимых решений о том где, куда и как они собираются перемещаться. Помимо того, такая карта позволит нам не увязнуть в путешествии коридор-за-коридором и комната-за-комнатой. 

> Однако некоторые ключевые области – вроде капитанского мостика – в тактических целях лучше все же проработать *(и прорисовать)* детально.

![Карта корабля ситхов][http://www.thealexandrian.net/images/20120502.jpg]

With this map we can now key both nodes and the routes between nodes. We can also allow characters to secure and/or barricade specific nodes or routes. (So some routes to the bridge may be less heavily guarded than others, for example.)

С такой картой, мы можем прописать её *«ключевые точки»* и продумать маршруты между ними. Мы так же можем позволить персонажам замаскировать и/или заблокировать те или иные узлы или маршруты *(сделав их неравноценными – прим. ред.)*. 

> Для примера, некоторые охраняемые маршруты на капитанский мостик могут охраняться менее качественно.

We can also assign travel times between locations.

*Так же мы можем присвоить то или иное время перехода между локациями для каждого из маршрутов.*

Shortcuts and Stealth Paths: Looking at our touchstones of Die Hard and Under Siege, we can see that a lot of the action is driven by protagonists seeking out alternative methods of moving around the structure (ventilation shafts, service corridors, burning holes through bulkheads, etc.).

### Короткие пути и тайные тропы

Глядя на главных героев «Крепкого орешка» и «В осаде» мы легко можем заметить привычку главных их героев искать альтернативные способы добраться из одной точки в другую. **Пролезть через вентиляционную шахту, пробраться по служебному коридору и проч.**

We could try including these routes onto our node map of the ship, possibly by using dotted lines:

Мы можем попробовать включить подобные маршруты в нашу карту корабля, например обозначив их пунктирными линиями.

![Карта корабля ситхов с секретными ходами][http://www.thealexandrian.net/images/20120502b.jpg]

Or, for a simpler and more flexible solution, we could assume that the ship has sufficient structural complexity that secret routes can always be found: With a sufficiently high skill check, a character can either find a short cut (which reduces the amount of time it takes to get from one location to another) or a stealth path (which makes it possible to reach areas of the ship that have been blocked off in one way or another).

Или, для простоты и гибкости, мы можем предположить, что судно имеет достаточно сложную структуру, в силу чего отыскать альтернативные пути можно всегда.

> Успешно пройдя проверку с повышенным уровнем сложности персонаж может найти короткий путь *(уменьшающий время путешествия из одной точки в другую)*, или скрытый от чужих глаз маршрут *(который, возможно, делает достижимыми ранее недоступные помещения)*.

To this, let’s add a wrinkle: If effort is taken, certain locations can be secured. For example, in Aliens the last of the survivors attempt to seal up a portion of the colony so that they can survive long enough for a rescue ship to arrive. Of course, the aliens still managed to find a way in, so let’s make that an opposed check: If your effort to find a stealth a path into my area is better than my check to seal the area, then you’ve found something that I forgot.

### Добавим деталей

**Дополнительное правило:** Если персонажи приложили должные усилия, та или иная зона может считаться защищённой.

Например, в фильме ["Чужие"][http://www.amazon.com/exec/obidos/ASIN/B004RE29PO/digitalcomics], последние из оставшихся в живых пытаются изолировать часть колонии до прилета спасательного судна. Конечно Чужие находят путь внутрь, однако давайте введём встречную проверку навыков для подобных случаев:

> Если ваш бросок на поиск лазеек лучше чем мой бросок на изоляцию зоны, то вам удалось найти что-то, что я упустил.

Control of the Ship: For a simple structure, we could simply equate control of the ship with control of the bridge. But if wanted a more dynamic scenario, we could make it so that individual systems can be taken offline or supersede the bridge’s control from other locations in the ship. (For example, you might be able to gain navigational control from the engine room; knock communications off-line by getting to the receiver room; turn off life support from environmental control; or access the automatic security systems from the security center.)

### Контроль над кораблем

Для упрощения структуры, мы можем приравнять взятие под контроль капитанского мостика к захвату всего корабля. 

Однако если мы хотим реализовать более сложный и интересный сценарий, мы можем реализовать возможность автономного управления отдельными подсистемами корабля, а также перехват контроля над судном извне мостика. 

> Например, вы можете дать возможность контролировать курс корабля из машинного отделения, отрубить дальнюю (и/или внутреннюю) связь из радиорубки, отключить систему жизнеобеспечения из рубки контроля микроклимата или получить доступ к системам автоматизированной обороны корабля из кабинета офицера безопасности.

Node Effects: On a similar note, we might want to define some specific game structures for special nodes. For example, if you can access the communications array and send a distress signal, what effect will that have?

### Эффекты места

Схожим образом, Вы, возможно, захотите назначить отдельные игровые структуры для некоторых конкретных моментов и мест игры.

- Например если вы можете проникнуть в узел связи и отправить сигнал бедствия, то какой эффект будет иметь это действие?

Is gaining control of the automatic security systems something that can be automatically achieved in the security center? Does it require an opposed skill check? A complex skill check? If so, how much time do those checks take? (Would it give enough time for the bad guys to physically lay siege to the security center?) Do you have to do it compartment by compartment? Do we run the whole thing as a massive hacker-vs-hacker battle in virtual reality?

- Даёт ли доступ в кабинет офицера безопасности возможность контроля систем безопасности судна? Требует ли это *(встречной)* поверки какого-либо навыка? Проверки нескольких навыков?  
- Потребуют ли эти проверки времени и дадут ли возможность плохим парням вломиться в помещение?   
- Будет ли взлом системы производиться слой за слоем?  
-  Или эта будет эпичная, хакер против хакера, битва в виртуальной реальности?

Do I prep the armory by simply having an inventory of available supplies? Or do we code something similar to a wealth check to see if a particular piece of desired equipment is available?

- Достаточно ли нам для подготовки оружейной комнаты судна простого перечня содержимого?   
- Или у нас будет отдельная проверка *(наподобие проверки Предусмотрительности/Богатства)* для поиска каждого понадобившегося предмета на этом складе?

Random Encounters: Finally, I’m always a big fan of using random encounters to simulate the activity in complex environments. Whether it’s panicked pockets of prostrate passengers or roving hijacker enforcement teams, a well-seeded random encounter table can add unexpected twists and delightful chaos to the scenario.

### Случайности

Наконец, как большой поклонник случайных встреч и событий, я всегда получаю массу удовольствия используя их в качестве симуляции внутриигровой активности. 

У вас паника среди пассажиров или вас штурмует отряд космических пиратов?

*Неважно!* Грамотно составленная таблица случайностей эффективно привнесёт в ваш сценарий дозу восхитительного хаоса и крутых поворотов сюжета.

OTHER SCENARIO STRUCTURES

## Прочие сценарные структуры

Here are some other potential scenario structures for a “Between the Stars” campaign.

Вот вам ещё несколько возможных сценарных структур для кампании в стиле *«Блуждая меж звёзд»*.

Bomb Onboard: Touchstones might include stuff like Die Hard With a Vengeance and Law Abiding Citizen. The actual mystery of identifying the bomber can probably use a standard mystery scenario structure, but what about the process of finding (and possibly defusing) bombs before they explode? What effect do bombs have on ship systems?

### Бомба

В качестве эксперимента вы можете использовать террориста-смертника, как в фильмах ["Крепкий орешек-2"][http://www.amazon.com/exec/obidos/ASIN/B000W4HIY0/digitalcomics] и ["Законопослушный гражданин"][https://ru.wikipedia.org/wiki/Законопослушный_гражданин]. 

- Обнаружение такого смертника может использовать стандартную структуру сценария-загадки, но что насчет поиска *(и, по возможности, разминирования)* установленных зарядов?   
- К чему приведет подрыв бомб на борту вашего судна?

Plague: The Babylon 5 episode “Confessions and Lamentations” is a particularly poignant look at disease scenarios on spacecraft. “Genesis” from Star Trek: The Next Generation was a particularly stupid one. (On the other hand, TNG’s “Identity Crisis” shows the breadth of potential within the general idea.)

### Эпидемия

Эпизод [Confessions and Lamentations](http://www.imdb.com/title/tt0517639/) сериала [«Вавилон-5»](https://ru.wikipedia.org/wiki/Вавилон-5) хорошо показывает нам особенности эпидемии в космосе. 

Эпизод [Genesis](http://en.wikipedia.org/wiki/Genesis_(Star_Trek:_The_Next_Generation)) из [Star Treck: The Next Generation](https://ru.wikipedia.org/wiki/Звёздный_путь), при этом, выглядит откровенно тупо.

Однако, [Identity Crisis](http://en.wikipedia.org/wiki/Identity_Crisis_(Star_Trek:_The_Next_Generation))из того же «ST:TNG» демонстрирует весьма интересную возможность развития идеи.

Lost in Space: Touchstones would include… well… Lost in Space. (Also, Poul Anderon’s Tau Zero.)

### Затерянные в космосе

В качестве эксперимента... ну... *«Затерянные в космосе»*. 

Здесь подойдут идеи из [Тау Ноль](https://ru.wikipedia.org/wiki/Тау_Ноль) Пола Андерсона.

Collision in Space: Here you could probably lift some of the same structures for systems damage from the “Bomb Onboard” scenario structure to model collision damage.

### Столкновение
Здесь можно попробовать применить ту же структуру повреждений судна что и в случае *«Бомбы»*.

Mutiny: We could probably lift large chunks from our “Hijacking” scenario structure

### Мятеж
Сюда можно вписать много всего из нашего примера структуры *«Захвата»*.

Smuggling: Both with the PCs engaging in smuggling and with NPC smugglers trying to use their ships to achieve their own aims.

### Контрабанда

*Контрабанда возможна как со стороны персонажей игроков, так и со стороны пассажиров, с их собственными целями.*

If you’re feeling up for it, grab one of these and give it the same treatment we gave hijackings: Post it here in the comments or toss a link down there to wherever you do post it.

Если вам близка тема контрабанды,  вам стоит проработать её так же, как я проработал сценарий захвата. 

И я был бы очень рад ссылке на результат вашей работы.

*To be continued...*

*[©The Alexandrian, 02.05.2012](http://thealexandrian.net/wordpress/15234/roleplaying-games/game-structures-part-14-scenario-structure-for-between-the-stars)*