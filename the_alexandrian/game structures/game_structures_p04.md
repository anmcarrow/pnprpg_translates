From: http://thealexandrian.net/wordpress/15147/roleplaying-games/game-structures-part-4-combat

Game Structures – Part 4: Combat April 9th, 2012
Go to Part 1

## test-test-test

Tied into the success of the dungeoncrawl is the success of traditional combat systems in roleplaying games. Although individual mechanics may vary, virtually all roleplaying games use a basic game structure for combat derived from D&D: Combat is divided into rounds in which everyone gets to take an action (or actions). Usually combat is further defined by an initiative system of some sort, so that you can easily answer the question, “Who goes next?”

If this sounds like the rigid structure of a boardgame or card game, that’s because it is: Derived from tabletop wargames, the average RPG combat system supplies clear-cut answers to the questions of, “What do I do?” and “How do I do it?”

Or, to break it down in a fashion similar to the dungeoncrawl:

Default Goal: Kill (or incapacitate) your opponents.

Default Action: Hit them.

Easy to Prep: Grab a bunch of monsters from the Monster Manual.

Easy to Run: The combat system breaks the action down into a specific sequence and usually provides a fairly comprehensive method of how each action should be resolved.

This is one of the reasons why so many roleplaying games focus so much mechanical attention on combat: No matter how much the players may be floundering, all you have to do is throw a couple of thugs at them and suddenly everyone at the table knows what to do. It’s a comfortable and easy position to default to.

It should also be fairly easy to see the almost perfect mesh between the macro-level structure of the dungeoncrawl and the micro-level structure of combat: When you pick an exit in the dungeoncrawl and find a room filled with monsters, you seamlessly switch into combat. When the combat has been resolved (and the room emptied of interest), you effortlessly return to the dungeoncrawl scenario structure and pick another exit.

Rinse, wash, repeat.

REWARDS

Combat also shows us how D&D links its systems of reward directly to its default game structures: The dungeoncrawl takes you to monsters, combat lets you defeat the monsters, and defeated monsters reward you with XP and treasure.

This is something which I believe other XP reward systems have generally overlooked: Many games have broken XP away from being a combat reward, but they haven’t reattached the XP award to another concrete game structure.

Whether or not that’s a desirable thing is a completely different discussion; a discussion involving Skinner boxes, the psychology of reward-driven pleasure, mechanical-reinforcement techniques, and a whole mess of other stuff.

But I think it’s an unexplored design space that would be interesting to turn some focus on. Particularly when you consider that (A)D&D has featured non-combat rewards for more than 20 years, and yet people still complain about all the XP in D&D coming from killing monsters.
