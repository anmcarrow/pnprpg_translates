**Связанные файлы:** game_structures_p11.md

**From:** http://thealexandrian.net/wordpress/4131/roleplaying-games/reactions-to-odd-experience-points

**Title:** Ответные действия в «OD&D»: Очки опыта(перевод)

![The Alexandrian splash](http://pnprpg.ru/wp-content/uploads/2014/08/the_alexandrian_splashes.png)

С оригиналом статьи Вы можете ознакомиться [здесь](http://thealexandrian.net/wordpress/4131/roleplaying-games/reactions-to-odd-experience-points). <!--more-->

---


Interesting fact about the basic rules for experience point awards in OD&D: They don’t actually exist.

Instead you have to intuit them out of an example on pg. 18 of Volume 1: Men & Magic, which states that you would get 7,700 XP for killing a troll with 7,000 gp of treasure: 7,000 XP for the 7,000 GP + 700 for killing the troll (which is a 7th level monster).

From this example you are forced to intuit that PCs receive 1 XP per gold piece of treasure and 100 XP per level of a defeated monster. (A monster’s level is basically determined by its Hit Dice.)

The one hard-and-fast rule regarding XP is that: “Gains in experience points will be relative; thus an 8th level Magic-User operating on the 5th dungeon level would be awarded 5/8 experience. [...] Experience points are never awarded above a 1 for 1 basis, so even if a character defeats a higher level monster he will not receive experience points above the total of treasure combined with the monster’s kill value.” But even this rule is somewhat vague, because in places it refers to “dungeon level” and in other places it refers to the “level of the monster”.

(It should be noted that the level of the dungeon was assumed to be correlated to its difficulty — with the first level of the dungeon appropriate for 1st level characters and so forth. But even in these early days it was acknowledged that harder foes could sometimes be found on upper levels and less powerful foes on lower levels, so there remains a real and meaningful difference.)

(This entire concept of adjusted XP would disappear from the game for awhile, before returning in 3rd Edition… where for reasons I’ve never quite been able to understand they mucked around with the math until it made no sense and required a chart look-up.)

Note, however, that the passage is clear that the adjustment is on a per character basis. The 8th level Magic-User has their experience adjusted, but if they were adventuring with a 4th level Fighter the fighter would not have their rewards so adjusted.

Writing this up now, I also just realized that there is also no provision given for dividing the experience award. If a party of 16 characters defeats a troll, they should all get the full XP award for it apparently. The players in my Caverns of Thracia one-shot are going to be pissed.

There’s also a recommendation that “no more experience points be awarded for any single adventure than will suffice to move the character upwards one level”, with an associated example suggesting that the character should max out 1 XP shy of gaining a second level from the same adventure.

 

XP FOR TREASURE

The practice of giving XP is much maligned. I criticized it myself when I was young. The logic usually goes something like this:

(1) “How does earning money improve your skills?”

(2) “Treasure itself is a reward. Why should you be rewarded for getting a reward?”

The answer is simple: Treasure was seen as an analog for accomplishment. The goal of the game was not, in fact, to go into a dungeon and fight with monsters. Fighting with monsters was, in fact, a really bad idea. Fighting monsters could get you killed. What you wanted to do was get the treasure without fighting the monsters.

By rewarding the bulk of XP for treasure, the game encouraged smart, strategic play instead of hack ‘n slash play. Combat was implicitly a means to an end, not the end itself. (I know that in the BECMI Basic Set, at least, it was explicitly made so. Whenever someone tries to tell you that D&D is a game about “killing things and taking their stuff”, keep that in mind.)

And this was intentional. Upon discovering that 100 XP per HD was encouraging players to treat monsters as a source of walking XP (instead of fearing them as deadly dangers), Gygax promptly revised the XP rules in Supplement 1: Greyhawk. Low level awards were drastically reduced (1 and 2 HD monsters, for example, were reduced to just 1/10th of their former reward) and experience awards were now explicitly divided among all party members. Hirelings and retainers were also given a full share (although they only benefited from half their portion).

Depending on how you read the rules, if you were in a group with a total of 10 characters (PCs and hirelings both) you could actually see your XP rewards for killing a 1 HD monster reduced to 1/100th its former level upon adopting the rules in Supplement 1: Greyhawk!

 

THE GENERAL PHILOSOPHY OF XP

This still leaves the objection that there’s no innate connection between finding a pot of gold and improving your sword-swinging ability. But this is almost utterly irrelevant because experience points — like virtually all character creation mechanics — are abstracted to the point of being virutally indistinguishable from a completely dissociated mechanic. Experience point awards are simply not any kind of meaningful model of actual learning or self-improvement in the real world — it doesn’t matter whether you give them for treasure, killing monsters, roleplaying, or just time served.

A few games (most notably RuneQuest) abandons them entirely and attempt to adopt associated mechanics that more meaningfully model the learning process. (For example, by improving skills that are used or trained.)

But if you choose to keep XP awards (and, like other dissociated character creation mechanics, I find nothing particularly problematic about them), then I think it’s important to acknowledge their role:

(1) They’re an efficient way of saying this is important. They can be an important part of the formal or informal social contract that says, “This is one of our primary goals.” If the primary source of XP is killing things, then you’re saying, “Killing things is going to be a focus of the game.”

(2) They’re a concrete way of setting and rewarding specific goals.

Of course, it’s also possible to over-emphasize the importance of these things. XP awards may feature an important part of the risk-vs-reward dynamic at the game table, but there are other rewards to be had — both in-character and out-of-character.

*[©The Alexandrian, 20.03.2009](http://thealexandrian.net/wordpress/4131/roleplaying-games/reactions-to-odd-experience-points)*