**Title:** Откровения (перевод)

**From:** http://arsludi.lamemage.com/index.php/41/revelations/

С оригиналом статьи Вы можете ознакомиться [здесь](http://arsludi.lamemage.com/index.php/41/revelations/).

---

Normal weapons can't kill the zombies. MicroMan doesn't trust Captain Fury. The lake monster is really Old Man Wiggins in a rubber mask.

*Обычное оружие не убивает зомби. Микромэн не верит Капитану Фьюри. Озёрное чудище на самом  деле – старик Виггинс в резиновой маске.*

These are Revelations. They are things you want the players to find out so that they can make good choices or just understand what is going on in the game. Revelations advance the plot and make the game dramatically interesting. If the players don't find them out (or don't find them out at the right time) they can mess up your game.

Всё это – Откровения.  
Всё это – вещи, которые ваши игроки должны найти чтобы принять верные решения или просто понять что же происходит в вашей игре. Откровения дополняют фабулу и делают её более драматичной.  
Если игроки не пересекутся с ними *(или не пересекутся с ними вовремя)*, это может испортить вам всю игру.

You have revelations in your game all the time whether you think about it or not, but you can improve your game by planning your revelations ahead of time instead of just letting them happen haphazardly. Lame Mage adventures like Dr Null: Battle on the Bay Bridge [free download] have clearly outlined revelations to make the GM's job easier.

В вашей игре всегда присутствуют Откровения, задумываетесь вы о них или нет. Однако, вы можете усовершенствовать вашу игру, заблаговременно продумывая их и не позволяя им возникать случайно. 

Приключения от «Lame Mage», наподобие [«Dr Null: Battle on the Bay Bridge»](http://www.lamemage.com/bridge.php), содержат четко прописанные Откровения, облегчающие труд воспользовавшегося ими ведущего.

Understanding revelations requires you to see things through the eyes of your players – what are they going to think at this point, will this be a surprise, will this seem consistent? As the GM you know lots of things the players don't. After you've spent days preparing a game, it's easy to become so absorbed in the details of your plot that you lose track of how it will unfold for your players.

Понимание того как правильно использовать Откровения, требует умения смотреть на вещи глазами игроков. 

>Что они подумают в этом месте? Станет ли это для них сюрпризом? Будет ли это для них логичным?

Write Your Revelations
An important revelation should be a critical point in the game, changing players' perceptions of the situation and possibly their response.

##Запишите ваши Откровения

**«Откровение должно быть важным, поворотным моментом игры, оно должно изменить мнение игроков о текущей ситуации и по возможности вызвать у них отклик».** © «Death of Dr. Null»

Writing out your revelations ahead of time shows you how the game is going to flow. Once play starts things can get a little hectic – you may accidentally have the evil mastermind show up and deliver his ultimatum and stomp off again without remembering to drop that one key hint that leads the heroes to his base. If you're lucky you recognize the omission and can backtrack. If you're unlucky you don't notice it at all, and you spend the rest of the game wondering why the players have such a different idea of what is going on than you do.

Запишите ваши Откровения загодя, до начала вашей игры.  

**В горячке начала вы можете что-нибудь упустить:** Например, показав вашего ГлавЗлодея, который предъявляет свой Ультиматум и топает восвояси, вы можете забыть обронить ключ, что приведёт героев к его убежищу.   
Если повезёт, вы заметите и исправите недочёт сразу.  
Если же нет, вы ничего не заметите и потратите какое-то время, удивляясь почему игроки никак не додумаются до того что же им нужно *(по вашему)*, делать.

Your list of revelations does not have to be detailed. In fact it's probably better to make each revelation a simple sentence you can absorb at a glance. Just writing “the Duke is now left-handed” on your list is enough: all the other details of who / what / where / why the Duke has been replaced by an impostor can be in the main body of the adventure. It should be a checklist or a trigger, not the whole explanation.

#####Ваш список Откровений не должен быть сильно детализированным
*В действительности, гораздо лучше ограничиться краткими шпаргалками.*  
Просто написать *«Герцог теперь левша»* – достаточно.
Все остальные детали того как, когда, где и почему Герцог был заменён самозванцем будет содержать само приключение.

Список должен содержать стартовые условия или триггеры, но не полноценные объяснения.

Moreover the revelation is not a canned moment: it doesn't say precisely how the fact is going to be revealed (though you may certainly have plans or expectations of how it's going to happen). This encourages you to remain flexible in-game, not script the whole thing. Players thrive on doing the unexpected, and a bare bones list of revelations lets you go with the flow but still refer back to your critical details. If the players did not go to the masque ball and see the Duke holding his wine glass in his left hand, you can see that “left-handed” is still on your revelations list and work it in some other way.

Кроме того, Откровения не привязаны к какому-то конкретному моменту. Они не говорят нам ничего о том как именно как именно они будут обнаружены *(хотя, конечно, у вас могут быть какие-то ожидания на этот счёт)*. 
Это обстоятельство вынуждает нас гибче вести игру и не пытаться уложить всё в жесткий сценарий. 
Игроки в таком случае получат удовольствие совершая непредсказуемые поступки, а ваш аскетичный список Откровений позволит вам держаться на одной волне с ними, не забывая о суть важных деталях.

> Даже если персонажи игроков не пойдут на бал-маскарад и не смогут увидеть там герцога, держащего свой бокал в левой руке, вы просто заглянете в ваш список Откровений и продемонстрируете герцога как-то иначе.

Notes
Of course the players may figure out more than you expect, sooner than you expect (they're precocious in that way). Your revelations are really the minimum the players should know at any point for the game to work.

##Примечания

Конечно, игроки могут всё понять куда быстрее и куда раньше чем вы ожидали *(это будет преждевременно и некстати)*.
Ваших Откровений, на такой случай, должно быть достаточно для того чтобы игра продолжалась.

Some revelations may be optional, particularly if they are clues that are not important if the PCs jump ahead and figure out the mystery without them. It doesn't matter if the PCs notice the duke is left-handed if they already figured out he's an impostor.

Некоторые откровения могут быть не обязательными, например если они не являются важными уликами, а персонажи уже выяснили всё и без них.  
>То что герцог левша неважно, если все уже поняли что он самозванец.

Revelations are also moments in play. The players may have guessed that Doc Oblivion really has an evil twin and may even be joking about it among themselves, but unless they talk about it in-character you still need the revelation moment for them to roleplay their reactions and establish it as a fact the characters know.

Помимо того, Откровения могут быть исключительно внуриигровыми моментами.
> Игроки могут догадываться о том, что у Доктора Забвение есть злой брат близнец, и могут даже шутить по этому поводу, но для того чтобы их персонажи смогли начать говорить об этом, всё равно нужен момент Откровения, с полноценным отыгрышем реакций и закреплением того факта что теперь персонажи Знают.