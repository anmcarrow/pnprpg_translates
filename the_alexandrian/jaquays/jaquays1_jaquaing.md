From: http://thealexandrian.net/wordpress/13085/roleplaying-games/jaquaying-the-dungeon

Jaquaying the Dungeon July 23rd, 2010
 
**title:** Подземелья в стиле Жако, ч.1

С оригиналом Вы можете ознакомиться [здесь](http://thealexandrian.net/wordpress/13085/roleplaying-games/jaquaying-the-dungeon)

I believe that dungeons should always be heavily jaquayed.

Я верю, что любое подземелье должно быть выдержано в стилистике[Жако](http://www.jaquays.com)
…

Okay, it’s true. I’m just making words up now.

Нет, в самом деле. Я всего лишь хочу вынести это на свет. 

In the case of jaquaying, the term is referring to Paul Jaquays, who designed Caverns of Thracia, Dark Tower, Griffin Mountain, and a half dozen other old school classics for Judges Guild, Chaosium, Flying Buffalo, and TSR before transitioning into video game design. 

В данном случае «Жако» - это фамилия [Поля Жако](http://en.wikipedia.org/wiki/Jennell_Jaquays), автора [Подземелий Трации](http://www.amazon.com/exec/obidos/ASIN/B000YBFHEE/digitalcomics),[Тёмной башни](http://en.wikipedia.org/wiki/Dark_Tower_(module)), [Горы Грифонов](http://www.amazon.com/exec/obidos/ASIN/B000F3N8SS/digitalcomics) и еще, как минимум дюжины олдскульных вещей для [«Judges Guild»](http://en.wikipedia.org/wiki/Judges_Guild), [«Chaosium»](http://www.chaosium.com), [«Flying buffalo»](http://www.flyingbuffalo.com) и [«TRS»](https://ru.wikipedia.org/wiki/TSR,_Inc.) ещё до того как занялся дизайном видеоигр.
<!--more-->
In the latter capacity he recently wrote some essays on maps he designed for Halo Wars:

Впоследствии он также написал несколько эссе к картам, которые он создал для [«Halo Wars»](http://www.amazon.com/exec/obidos/ASIN/B0034JKYO6/digitalcomics).

Memorable game maps spring from a melding of design intent and fortunate accidents.
Paul Jaquays – Crevice Design Notes


> *Запоминающиеся игровые карты возникают из сочетания первоначального дизайна и счастливых случайностей.
> © Поль Джако - [Заметки о золотой жиле дизайна](http://www.halowars.com/GameInfo/maps/crevice.aspx)*

That’s timeless advice, and a design ethos which extends beyond the RTS levels he helped design for Halo Wars and reaches back to his earliest work.

Именно этот неустаревающий совет и сама эта манера дизайна, простирающаяся далеко за пределы карт для RTS, помогли ему в работе над «Halo Wars». Истоки всего этого лежат в более ранних его работах.

What Jaquays particularly excelled at in those early Judges Guild modules was non-linear dungeon design.

### Что действительно восхищает в ранних сюжетных модулях Жако, так это нелинейный дизайн подземелий.

For example, in Caverns of Thracia Jaquays includes three separate entrances to the first level of the dungeon. And from Level 1 of the dungeon you will find two conventional paths and no less than eight unconventional or secret paths leading down to the lower levels. (And Level 2 is where things start getting really interesting.)

Например, в «Подземелья Трации» Жако включил три различных способа для входа на первый уровень подземелья. 
А уже на этом первом уровне можно найти два общеизвестных и не менее восьми малоизвестных, скрытых или замаскированных спусков на нижние уровни. 
> Таким образом, начиная со второго уровня подземелье становится всё интереснее и интереснее.

The result is a fantastically complex and dynamic environment: You can literally run dozens of groups through this module and every one of them will have a fresh and unique experience.

#### Результатом становится невероятно цельное и динамичное окружение
Вы можете провести, буквально, несколько дюжин игровых групп через этот модуль, и для каждой из них это путешествие будет необычным и уникальным.

But there’s more value here than just recycling an old module: That same dynamic flexibility which allows multiple groups to have unique experiences also allows each individual group to chart their own course. In other words, it’s not just random chance that’s resulting in different groups having different experiences: Each group is actively making the dungeon their own. They can retreat, circle around, rush ahead, go back over old ground, poke around, sneak through, interrogate the locals for secret routes… The possibilities are endless because the environment isn’t forcing them along a pre-designed path. And throughout it all, the players are experiencing the thrill of truly exploring the dungeon complex.


#### Однако, подобная структура имеет бОльшую ценность нежели простая возможность переработки старого сюжета

Та же самая гибкость и динамичность, которая позволяет  нескольким группам в рамках одной карты получать уникальный игровой опыт, позволяет каждой отдельной группе самостоятельно выбирать свой собственный путь.

#### Иными словами, разница опыта у разных групп не является случайностью
Каждая группа, в рамках такой структуры активно формирует свое собственное подземелье. 

Они могут постоянно возвращаться на одно и то же место, нарезать круги, тщательно исследовать каждый закуток, пропускать те или иные части, искать обходные пути, открывать новые секретные ходы... *Их возможности безграничны, так как само окружение не стремиться загнать их на единственно-верный, продуманный до мелочей маршрут.* 

На протяжении всего игрового процесса, у них есть возможность испытать дрожь аутентичного исследования гигантского подземного комплекса *(вне зависимости от того сколько раз и под какой личиной они в него попадают - прим. переводчика)*.

By way of comparison, Keep on the Shadowfell is an extremely linear dungeon:

В проитвовес тому, [Keep on the Shadowfell](http://www.amazon.com/exec/obidos/ASIN/0786948507/digitalcomics) – это чрезвычайно линейное подземелье

![sample Keep of Shadowfell](http://www.thealexandrian.net/creations/misc/jaquay-dungeon/kots-jaquay1.jpg)

(This diagram uses a method laid out by Melan in this post at ENWorld.)

> Эта диаграмма построена по методу, Melan, [подробно описанному им на на «ENWorld»](http://www.enworld.org/forum/general-rpg-discussion/168563-dungeon-layout-map-flow-old-school-game-design.html).

Some would argue that this sort of linear design is “easier to run”. But I don’t think that’s actually true to any appreciable degree. In practice, the complexity of a jaquayed dungeon emerges from the same simple structures that make up a linear dungeon: The room the PCs are currently in has one or more exits. What are they going to do in this room? Which exit are they going to take?

Некоторые утверждают что подобный линейный дизайн «проще для прохождения». Но я не считаю что это хоть насколько-нибудь верно. 

*На практике, сложные и комплексные подземелья «в духе Жако»  создаются на базе тех же простых структур что и аналогичные линейные подземелья.* 

> Помещение, в котором оказались герои, имеет один или больше выходов. 
> * Что они собираются делать в этой комнате?
> * Каким из выходов они воспользуются?

In a linear dungeon, the pseudo-choices the PCs make will lead them along a pre-designed, railroad-like route. In a jaquayed dungeon, on the other hand, the choices the PCs make will have a meaningful impact on how the adventure plays out, but the actual running of the adventure isn’t more complex as a result.

В линейных подземельях игрокам предоставляется иллюзия выбора. Когда они выбирают одну из альтернатив, то все равно следуют прямому, как железнодорожные рельсы, заранее созданному сценарию.

В противоположность тому, подземелья Жако позволят выбору героев сильно влиять дальнейший ход игры, 
*но это никоим образом не сделает саму игру более трудоёмкой.*

On the other hand, the railroad-like quality of the linear dungeon is not its only flaw. It eliminates true exploration (for the same reason that Lewis and Clark were explorers; whereas when I head down I-94 I am merely a driver). It can significantly inhibit the players’ ability to make meaningful strategic choices. It is, frankly speaking, less interesting and less fun.

С другой стороны, рельсоподобные, имеющие линейную структуру подземелья имеют не только этот недостаток. 

Они также исключают процесс аутентичного исследования. 

> В том же смысле, в котором [Люис и Кларк](http://en.wikipedia.org/wiki/Lewis_and_Clark_Expedition) были исследователями, а я, едущий по трассе I-94, являюсь всего лишь заурядным водителем.

Такое свойство линейных подземелий может в значительной степени подавлять способность игроков к стратегически значимому выбору. 

А это, между нами говоря, и не так интересно, и не так весело.

So I’m going to use the Keep on the Shadowfell to show you how easy it is to jaquay your dungeons by making just a few simple, easy tweaks.

Итак, далее я собираюсь использовать «Keep of the Shadowfell» для того что бы наглядно продемонстрировать Вам то, как используя несколько элементарных приемов и трюков можно произвести Жако-изацию  этого  *(изначально линейного и скучного – прим. ред.)* подземелья.


http://i.imgur.com/wdpF8.jpghttp://img.acianetmedia.com/i/DeNWV.jpg