from: http://thealexandrian.net/wordpress/13144/roleplaying-games/jaquaying-the-dungeon-addendum-part-2-tips-tricks

Jaquaying the Dungeon – Addendum, Part 2: Tips & Tricks August 27th, 2010
Go to Part 1: Dungeon Level Connections

COMBO PLATTER: Elevators that lead to underground rivers. Ladders that take you through imperceptible teleportation effects. Stairs that end in a sloping passage.

Such combinations of multiple level connector types can be as complicated as you’d like: For example, an elevator shaft that has been blocked by the adamantine webs of a lavarach. This requires the PCs to climb down the shaft (like a chute), clear the webs (like a collapsed tunnel), and then reactivate the elevator mechanism (allowing it to be used as such in the future).

ONE CONNECTOR, MULTIPLE LEVELS: An elevator can stop at several floors. A flight of stairs can provide exits to many different levels. A single room might contain multiple teleportation devices, or a single teleportation device might lead to different locations at different times of the day.

INVISIBLE TRANSITIONS: The PCs swap levels without realizing that it’s happened. These can be the result of mundane effects (like a gently sloping passage), but are perhaps more frequently magical in nature (imperceptible teleportation effects). In dungeons rich with minor elevation shifts, the PCs may even baffle themselves by mistaking an obvious level connector (like a staircase) for a minor adjustment in the elevation on the same level.

FALSE STAIRS: In their section on “Tricks and Traps”, Arneson and Gygax refer to “false stairs” without any real explanation of what they mean. I’m going to use the phrase to mean the opposite of invisible transitions: False stairs are features of the dungeon which lead the players to believe they have moved to a new level of the dungeon when they haven’t actually done so. Minor elevation shifts frequently fall into this category, but so can more deliberate deceptions. (For example, an elevator wrapped in illusions to make the PCs believe they’re descending, but which actually releases them back onto the same level they started on.)

MISLEADING STAIRS: Connectors which initially look as if they’ll take you in one direction before actually heading in the opposite direction. For example, a flight of stairs that go up one level to a sloping passage that goes back down two levels.

ONE-WAY PATHS: Teleportation devices are perhaps the most common example of one-way paths, but more mundane traps and hazards can also have the same result. For example, a flight of stairs that turns into a slide. Or an underground river that sweeps PCs away in a torrential current.

REMOTE ACTIVATION: A path that only becomes available once it has been activated from some remote location. For example, a lever which opens a stone panel and provides access to a staircase. Or a teleportation system which must be properly aligned.

Remote activation also implies the possibility for remote deactivation, either stranding the PCs with no possibility of retreat or removing familiar paths that were taken in the past.
