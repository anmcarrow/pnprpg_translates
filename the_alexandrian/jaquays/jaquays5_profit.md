**title:** Подземелья в стиле Жако, ч.5 – К вящей пользе и пущему веселью (перевод)

![The Alexandrian splash](http://pnprpg.ru/wp-content/uploads/2014/08/the_alexandrian_splashes.png)

[Ссылка на первую часть цикла](http://pnprpg.ru/blog/2014/12/01/thealexandrian-jaquaying-the-dungeon-1)

С оригиналом Вы можете ознакомиться [здесь](http://thealexandrian.net/wordpress/13132/roleplaying-games/jaquaying-the-dungeon-part-5-jaquaying-for-fun-and-profit)

---

We started with a linear dungeon:

[Как вы помните](http://pnprpg.ru/blog/2015/03/23/thealexandrian-jaquaying-the-dungeon-4/), разбирая «Крепость теней» мы начали с линейной структуры подземелья. <!--more-->

![](http://www.thealexandrian.net/creations/misc/jaquay-dungeon/kots-jaquay1.jpg)

But after jaquaying the Keep, the result is this:

Но после небольшой жакоизации всё стало совсем иначе:

![](http://www.thealexandrian.net/creations/misc/jaquay-dungeon/kots-jaquay2.jpg)

Note that we haven’t changed the actual key to the adventure: We’ve just restructured the environment in which those encounters are placed.

Заметьте, мы даже не изменяли само приключение, мы всего лишь перестроили пространство, в котором оно происходит.

I’ve also prepped some detail-light maps to make the changes a little clearer. You’ll want to cross-reference with the maps from the original module. (The original Level 2, which is now Level 3, is unchanged, so I didn’t re-map it.)

### Вот вам пара схематичных карт, с моей версией «Крепости»
При желании,вы легко можете угадать в них детали первоначальной карты.

*Уровень 2, который после жакоизации стал уровнем 3, остался неизменным. Поэтому я не стал изображать его повторно.*

![](http://www.thealexandrian.net/creations/misc/jaquay-dungeon/kots-jaquay-level1.jpg)

![](http://www.thealexandrian.net/creations/misc/jaquay-dungeon/kots-jaquay-level2.jpg)

I’ll take a moment to note that this isn’t the only way we could have done this. Other things we could have done:

Пользуясь моментом, замечу, что произведённые мной изменения – не  единственный возможный способ жакоизации «Крепости». 

### Ещё мы могли бы...

•    Put a secret door at the bottom of the pit trap in area 1 (leading to one of several possibilities on the second level).

* Добавить секретную дверь на дне ловушки, в точке 1. Это бы добавило еще один проход на второй уровень.

•    Have the kruthiks tunnel from area 10 down to area 15.

* Провести сделанный крутиками [(kruthiks)](http://en.wikipedia.org/wiki/Kruthik) тоннель из точки 10 в точку 15.

•    Put a teleport in Sir Keegan’s tomb keyed to a matching crypt on the second level.

* Разместить в гробнице сэра Когана телепорт, на второй уровень.

•    One of the prisoners in the torture chamber dug a hidden escape tunnel leading to area 6 (where he was killed by zombies, the poor bastard).

* *Один из заключенных в камере пыток нашёл секретный тоннель ведущий в точку 6. Где и был убит зомби. Вот же несчастный ублюдок!*

•    Could there be a connection between the pool in area 11 and the water-based trap in area 16?

* Почему бы не соединить бассейн в точке 11 с водяными ловушками в точке 16?

•    Could the access to area 15 north of area 16 be a secret door, with a more obvious entrance leading from area 17 (allowing meticulous PCs to potentially bypass the trap)?

* А ещё можно сделать закрытый секретной дверью проход между точкой 15 и 16, причём проход этот будет легко найти в точке 17? *Это бы позволило дотошным персонажам обойти ловушки.

The particular revisions I’ve made simply struck me as either the most interesting or the most appropriate or both.

Как бы-то ни было, те изменения, что были  сделаны, были выбраны мной либо за свою простоту, либо – за интересность, либо за то и за другое, одновременно.

But the point of performing this revision on Keep of the Shadowfell is not only to salvage another aspect of this adventure. My primary goal is to demonstrate how easy it is to implement these techniques in your own dungeons. If we can take an existing, linear dungeon and fundamentally transform it in just a couple of minutes using a handful of jaquaying techniques, then the effect can be even more dramatic if we were to design a dungeon from the ground-up using those techniques.

Основной же смысл произведённой мной переделки «Крепости» заключался не только, и не столько, в том, чтобы улучшить это подземелье, сохранив его узнаваемость. 

Главной моей задачей было – продемонстрировать то, насколько просто вы можете применять техники жакоизации для ваших собственных подземелий. 

Ведь, как видите, даже если мы возьмем готовое подземелье с линейной структурой, его фундаментальное преобразование при помощи толики техник Жако всё равно будет делом пары минут. 

Тем более впечатляющим может быть конечный эффект, если вы  создадите своё подземелье с самого начала используя эти техники.  

Here’s a quote from a recent interview with Paul Jaquays over on Grognardia:

### Вот вам цитата из одного интервью с Полом Жако
(взято с <grognardia.blogspot.com>)

The core inspirations for Caverns of Thracia were threefold. The first was to ally the various “beast” races of AD&D as a unified force. The second was to build encounters that took place in multiple levels of a cave, where the open upper areas were situated above open lower areas. The final inspiration (that I remember) was the rather primitive, but unique plate armor used by Mycenaean soldiers. These became the human guards of the upper reaches of the Caverns.

> Создавая «Подземелья Трации», я черпал вдохновение из трёх источников:  
> - Первый состоял в идее объединения различных «диких» рас AD&D в одну унифицированную фракцию;
> - Второй - в идее организации случайных встреч и игровых событий моего подземелья, так, чтобы они могли одновременно происходить на нескольких уровнях, что стало возможным через прямое совмещение *(колодцы, провалы, трещины, и т.п. - прим. ред.)* некоторых помещений верхних уровней с помещениями расположенными прямо под ними;  
> - И, наконец, третьим источником стал, насколько помню, весьма примитивный, но оригинальный древнегреческий доспех из Микен. Именно такие доспехи, в итоге, носили стражи на верхних уровнях «Подземелий».

Of particular interest here is Jaquay’s second inspiration: I can personally testify to the effectiveness of these open caverns in transforming the typical dungeoncrawling experience. They immediately force the players to think in three dimensions, while their ubiquity significantly contributes to the memorable layout of the dungeon.

#### Практический интерес здесь, более всего, представляет второй пункт

Я могу засвидетельствовать однозначную действенность подобного совмещения уровней, и то, что оно приводит к существенной трансформации типичного процесса зачистки подземелий.

Появление точек (мест) совмещения уровней немедленно вынуждает  игроков переключаться в трёхмерный режим восприятия игрового пространства, а их действия начинают вносить куда более значительный вклад в жизнь всего подземелья.

![](http://www.thealexandrian.net/creations/misc/jaquay-dungeon/kots-jaquay10.jpg)

But the important revelation to be had here, in my opinion, is the effectiveness of clearly delineating a small list of concrete creative goals before beginning your dungeon design.

Однако, самым важным тут, на мой взгляд, является принцип составления краткого списка тех целей, к которым вы будете стремиться в дальнейшем, в процессе конструирования подземелья.

Building on that point, notice that Jaquays only specifies a single non-linear design technique in his list of creative goals. (And it’s actually a very specific variation of a generalized technique.) And although that is not the only non-linear technique employed in the Caverns of Thracia, Jaquays’ riffs on that theme are a definitive aspect of the module.

Учитывая эту идею, обратите внимание на то, что Жако указал только одну из техник нелинейного дизайна подземелья в своём списке принципов организации «Подземелий Трации». 
*Причём, способ реализации этой техники здесь сильно отличается её базовой формы.*

И хотя конкретно эта техника - не единственная из тех что он использовал в дизайне «Подземелий», Жако уделяет ей особое внимание и делает её определяющей для всего своего подземного комплекса.

Here’s my point: Earlier in this series, I listed a dozen jaquaying techniques. Next time you’re designing a dungeon, don’t feel like you need to cram ‘em all in there. Instead, pick one of them and try to explore it in as many ways as possible while you’re designing the dungeon. (If you want a more focused experience, follow Jaquays’ example and try to narrow your design theme down a specific variant of one technique – just like multi-level caverns are a specific form of unusual level connectors.)

### К чему я клоню?
Ранее в этой серии, я осветил добрую дюжину техник жакоизации подземелий.  
В следующий раз, когда вы будете создавать собственное подземелье, не старайтесь засунуть в него все эти техники одновременно. 

Вместо того, попробуйте выбрать какую-нибудь одну и постарайтесь сделать её определяющей темой вашего комплекса. 

> Ну и в целом, если вы хотите целенаправленно развивать свои навыки дизайнера подземелий, то имеет смысл сделать как Жако – сократить список используемых методов до одного, а потом  использовать его максимально полно и разнообразно. 
> Так, в случае «Подземелий Трации», множественные межуровневые каверны и колодцы являются всего-лишь развитием идеи [«неудобных маршрутов»](http://pnprpg.ru/blog/2014/12/13/thealexandrian-jaquaying-the-dungeon-2/).

Jaquaying your dungeon is easy. It’s also fun. And this applies to both the designing of the dungeon and the playing of the dungeon. Nothing is more exciting for me as a GM that to sit down at the table and know that I’m going to be just as surprised by my players as I hope that my players will be by me.

В общем, жакоизировать ваше подземелье можно несложно и весело. 
Причём, это замечание одинаково актуально и в случае карты подземелья, так в случае самого процесса игры. 

Для меня, как для ведущего, ничто не является более захватывающим опытом, чем ожидание того, чем удивят и порадуют меня за игровым столом мои игроки, а также того, чем я удивлю их в ответ.

And when it comes to dungeon design, that’s the unique and exciting experience that jaquaying unlocks.

И, когда речь идёт именно о дизайне подземелий, техники Жако - как раз то, что даёт нам этот самый захватывающий игровой опыт.

(http://thealexandrian.net/wordpress/13132/roleplaying-games/jaquaying-the-dungeon-part-5-jaquaying-for-fun-and-profit)