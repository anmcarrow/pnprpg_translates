**From:** http://thealexandrian.net/wordpress/8032/roleplaying-games/node-based-scenario-design-part-7-more-alternative-node-designs

**Title:** Node-Based Scenario Design – Part 7: More Alternative Node Designs 

![The Alexandrian splash](http://pnprpg.ru/wp-content/uploads/2014/08/the_alexandrian_splashes.png)

С оригиналом статьи Вы можете ознакомиться [здесь](http://thealexandrian.net/wordpress/8032/roleplaying-games/node-based-scenario-design-part-7-more-alternative-node-designs)


	LARGE LAYERS
	
	In case it hasn’t been clear, I’ve been using three-node layers in these examples because it’s a convenient number for showing structure. But there’s nothing magical about the number. Each “layer” in the previous examples constitutes an interlinked environment (either literal or metaphorical) for exploration or investigation, and you can make these environments as large as you’d like.
	
	As long as each node has a minimum of three clues in it and a minimum of three clues pointing to it, the Three Clue Rule and its inversion will be naturally satisfied and guarantee you a sufficiently robust flow through the layer. But as you increase the number of nodes, you also open the possibility for varying clue density: Particularly dense clue locations could have six or ten clues all pointing in different directions.
	
	Obviously, however, the larger each layer is, the more prep work it requires.
	
	DEAD ENDS
	
	Dead ends in a plotted mystery structure are generally disasters. They mean that the PCs have taken a wrong-turn or failed to draw the right conclusions and now the train is going to crash into a wall: There should be a clue here for them to follow, but they’re not seeing it, so there’s nowhere to go, and the whole adventure is going to fall apart.
	
	But handled properly in a node-based structure, dead ends aren’t a problem: This lead may not have panned out, but the PCs will still have other clues to follow.
	
	Node-Based Scenario Design - Dead Ends
	
	In this example, node E is a dead end. Clues at nodes B and C suggest that it should be checked out, but there’s nothing to be found there. Maybe the clues were just wrong; or the bad guys have already cleared out; or it looked like a good idea but it didn’t pan out into usable information; or it’s a trap deliberately laid to catch the PCs off-guard. The possibilities are pretty much limitless.
	
	The trick to implementing a dead end is to think of clues pointing to the dead end as “bonus clues”. They don’t count towards the maxim that each node needs to include three different clues. (Otherwise you risk creating paths through the scenario that could result in the PCs being left with less than three clues. Which may not be disastrous, but, according to the Inverse Three Clue Rule, might be.)
	
	On the other hand, as you can see, you also don’t need to include three clues leading to a dead end: It’s a dead end, so if the PCs don’t see it there’s nothing to worry about.
	
	Of course, if you include less than three clues pointing to the dead end then you’re increasing the chances that you’re prepping content that will never be seen. But this also means that the discovery of the dead end might constitute a special reward: Extra treasure or lost lore or a special weapon attuned to their enemy.
	
	Which leads to a broader point: Dead ends may be logistical blind alleys, but that doesn’t mean they should be boring or meaningless. Quite the opposite, in fact.
	
	In the same vein as dead ends, you can also use “clue light” locations. (In other words, locations with less than three clues in them.) Structurally such locations generally work like dead ends, by which I mean that clues pointing to clue light locations need to be “bonus clues” to make sure that the structure remains robust.
	
	The exception to this guideline is that you can generally have a number of two-clue locations equal to the number of clues accessible in a starting node. (For example, in the layer cake structure diagram you have three nodes in the bottom layer with only two clues each because the starting node contains three clues. If there wasn’t a starting node with three clues in it, the same structure would have potential problems.)
	
	LOOPS
	
	Node-Based Scenario Design - Loops
	
	In this simple loop structure all four nodes contain three clues pointing to the other three nodes. The advantage of this simple structure is that the PCs can enter the scenario at any point and navigate it completely.
	
	Obviously, this is only useful if the PCs have multiple ways to engage the material. In a published product this might be a matter of giving the GM several adventure hooks which can be used (each giving a unique approach to the adventure). In a personal campaign, clues for nodes A, B, C, and D might be scattered around a hexcrawl: Whatever clues the PCs find or pursue first will still lead them into this chunk of content and allow them to explore it completely.