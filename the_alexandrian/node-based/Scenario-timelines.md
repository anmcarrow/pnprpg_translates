**From:** http://thealexandrian.net/wordpress/4154/roleplaying-games/dont-prep-plots-prepping-scenario-timelines
**linked papers**: 
http://thealexandrian.net/wordpress/580/roleplaying-games/tales-from-the-table-in-the-depths-of-khunbaral
http://thealexandrian.net/wordpress/991/roleplaying-games/tales-from-the-table-unexpected-successes
**Title:** Не придумывайте сюжет! Пример сценария.

	This is an example of prepping a detailed scenario timeline, an important part of scenario-based prep as described in Don’t Prep Plots. It’s taken from my current campaign, In the Shadow of the Spire.
	
	MY PLAYERS SHOULD STOP READING
	
	Night of Dissolution - Monte CookThe basic scenario is the “Temple of Deep Chaos”, which is part of Monte Cook’s Night of Dissolution, a series of interconnected adventures set in the city of Ptolus. All you need to know for the purposes of this example is that several groups of cultists — the Ebon Hand, the Brotherhood of Venom, and the Cult of the Tolling Bell — have joined forces to conduct a series of vile experiments. They have taken over an apartment building and have been experimenting on the former residents, transforming them into venom-shaped thralls.
	
	In the depths beneath the apartment complex, there is an extensive temple complex dedicated to the ancient gods of chaos.
	
	INITIAL CONDITIONS
	
	When the PCs first arrive at the apartment complex, tensions are rising between the three cults. In the original adventure, in fact, two members of the Brotherhood of Venom have killed an Ebon Hand cultist on the second floor of the apartment complex just moments before the PCs arrive. When the PCs eventually open the door to this room on the second floor, they’re confronted by the two murderers stooping over the body of their victim.
	
	Wanting to make things a bit more dynamic, I decided to alter this set-up slightly: The fight between the cultists would start a few minutes after the PCs first entered the apartment complex. I had the vague idea that they would hear the sounds of combat coming from upstairs and be able to either investigate it or avoid it at their choosing.
	
	Fortunately, I didn’t spend too much time thinking about it because that isn’t even remotely what happened. Instead two of the PCs snuck into the neighboring apartment building and used clairvoyance to spy on the compound. This triggered the murderous cultists and started the ball rolling. The PCs then left the area.
	
	In addition, one of the PCs (Tee) had ended up working for the cultists. From midnight to 6 AM every day, she kept a surrepitious watch on the apartment building from an alley across the street.
	
	 
	
	FIRST TIMELINE
	
	As part of the prep for my campaign, I maintain a Campaign Status document. This is my one-stop shop for (a) daily news on the street; (b) upcoming events; (c) the activities of major NPCs; (d) the status of dangling plot threads; and (e) the developing status of hot spots in the campaign (like this apartment complex).
	
	So as part of my prep following the session in which they witnessed the murder (and started events rolling at the apartment complex), I wrote up the following timeline of events:
	
	9/15/790: Audon and Uranik (BV) kill Reggaloch and leave his body in the apartment complex.
	
	9/17/790: Audon and Uranik (BV) return to their quarters during the day.
	
	9/18/790: Reggaloch’s body is discovered during Tee’s shift by Theral (BV). This causes a minor commotion as various BV priests from the Sewers and Deep Temple come up. Gavele eventually comes up, has the body dumped into a venom-shaped thrall area (to be eaten) and gets the priests under control.
	
	9/18/790: Near the end of Tee’s shift, Rhinnis leaves the apartment complex and meets with a man on the street. He gives him a letter (which is taken to the Temple of the Ebon Hand, telling them what’s happening).
	
	9/18/790: When Uranik leaves during the day, Rhinnis lures the other two BV priests to the Long Passage and kills them.
	
	9/19/790: During Tee’s shift, three EH from the temple enter the apartment building. They go down to the Deep Temple, meet up with Rhinnis, and attack Audon. Audon attempts to escape. He’s killed in the sewers, but not before Theral, Zlith, and Grealden are warned and attempt to escape. They reach the apartment complex. Zlith is killed along with one of the EH priests. Theral and Grealden reach the street, the EH pursue them into the street and kill them. Then Rhinnis and Vocaetun go back inside, while the others grab the BV bodies and skedaddle.
	
	9/19/790: Uranik returns during Iltumar’s shift and is ambushed by Rhinnis in the apartment complex. With the help of venom-shaped thralls, Rhinnis subdues Uranik and then feeds him askara.
	
	9/20/790: Three EH priests with stats identical to the BV priests arrive in the Sewers to reinforce Vocaetun. (The complex essentially stabilizes under EH/TB control.)
	
	The names and many of the details may be meaningless without the larger context of the scenario, but hopefully the gist of it is clear: If the PCs do absolutely nothing, these are the events that happen, culminating on 9/20/790 with the complex settling into a new status quo. The fact that Tee would probably be watching the complex meant that I had to be relatively specific about what she would and wouldn’t see, but even if she hadn’t been there I still would have laid out a timeline with a similar amount of daily detail.
	
	(The new status quo was a convenient stopping point, but even without that I wouldn’t have bothered planning much further in advance. I thought it very likely that the PCs would return to the apartment complex within a few days and, thus completely change the situation. But even if they didn’t, there was no sense prepping stuff that wouldn’t happen for at least three or four sessions.)
	
	 
	
	SECOND TIMELINE
	
	In the wee hours of 09/18/790, Tee observed the commotion that started when Reggaloch’s body was found. Using boots of levitation she snuck up to a window and watched the whole thing unfold.
	
	Later that day, the PCs did, in fact, return to the complex and wreak havoc. They raided the apartment building and began destroying the research. I decided, based on my timeline, that Uranik would come up out of the temple midway through their raid. They capture Uranik and another cultist named Arveth, question them, knock them unconscious, and leave them in the sewer. Then they started a raid on the temple. Zlith, Grealdan, and Vocaetun were killed. A garrison of ratmen was wiped out. Theral fled into the sewers.
	
	The PCs were eventually forced to retreat, but by that point everything had changed. This prompted the creation of my second timeline:
	
	9/15/790: Audon and Uranik (BV) kill Reggaloch and leave his body in the apartment complex.
	
	9/17/790: Audon and Uranik (BV) return to their quarters during the day.
	
	9/18/790: Reggaloch’s body is discovered during Tee’s shift by Theral (BV). This causes a minor commotion as various BV priests from the Sewers and Deep Temple come up. Gavele eventually comes up, has the body dumped into a venom-shaped thrall area (to be eaten) and gets the priests under control.
	
	9/18/790: When Uranik leaves during the day, Rhinnis lures the other two BV priests to the Long Passage and kills them.
	
	9/18/790: The PCs wreak havoc.
	
	9/18/790: Rhinnis, returning from the Long Passage, discovers the battle on the lower level. He decides to temporarily cover it up; stacking the bodies in area 3. He goes up into the sewers and repairs the hole in the floor of the apartment complex (using a little manual labor and then his mending spell).
	
	o Uranik has escaped. Arveth is taken to the Upper Temple complex and kept tied up in area 7.
	
	o The City Watch surrounds the apartment complex for an hour before moving in. They find the disgusting remnants of vile rituals, but no current activity. They board up the entrance and place a standard guard of two watchmen until 9/22/790 when an “outraged” House Vladaam reclaims its property.
	
	9/19/790: During Tee’s shift, Rhinnis intercepts three EH from the temple a couple blocks away from the apartment complex. He sends one of them back to get more and then takes the others down to the upper temple.
	
	o Rhinnis returns to the Deep Temple and reports to Illadras. Illadras agrees to his plan to let the EH take control of the upper temple with the goal of reconstructing their research.
	
	o Illadras gives Rhinnis a letter for Wuntad. Rhinnis gives it to one of the EH priests, who takes it to Porphyry House.
	
	o Arveth is tortured and one of her eyes plucked out before they believe she’s telling the truth. Then she takes point on gathering up the remaining watchers and reassigning them to looking for Laurea.
	
	o Audon is taken and force-fed askara in area 7 of the Deep Temple.
	
	09/20/790: The upper temple is reinforced by the EH.
	
	o 1 elder priests; 5 priests; 6 guards
	
	o Half the rat garrison is replenished from the TRG
	
	o Audon cocoons in area 7. Additional EH priests arrive and begin studying the remaining askara sample and the cocoon in an effort to recreate the lost research (they meet with little success, but a masterwork alchemist’s lab is erected).
	
	09/21/790: The other half of the rat garrison is replenished from the TRG. Rhinnis moves into area 3 of the Deep Temple.
	
	o Audon emerges as a venom-shaped thrall in area 7.
	
	o Wuntad sends a letter back to Illadras regarding the evolving situation.
	
	09/22/790: Rhinnis moves the venom-shaped thrall to area 1.
	
	09/24/790: Dilar and a group of BV priests attack the upper temple to avenge themselves on the EH cultists who betrayed them (which is sort of true).
	
	I use strike-thru text for events that have already happened. I used to simply remove past events from the timeline, but I found that there were times when I needed to reference that information. The strike-thru lets me still read it if I need to, but clearly distinguishes it from the stuff I need to be paying attention to for the current session.
	
	This timeline, you’ll note, has a lot more detail to it. Because the PCs so thoroughly disrupted the cult’s activities and defenses, it’s important for me to know exactly what state those defenses are in when they return — whether that’s an hour after they left, a day after they left, or a week after they left. By the 22nd, those defenses have pretty much stabilized and the compound has reached a new status quo.
	
	I actually have no idea how the Dilar-led attack on the 24th will turn out. My guess is that I’ll never have to worry about it because the PCs will return to the temple before it happens. (Of course, even if the PCs wipe out the temple, Dilar and the Brotherhood of Venom will still be out there. Maybe I’ll have them launch their attack of vengeance on the Temple of the Ebon Hand directly. That could be nifty.)
	
	In any case, if the PCs interfere with these events again I may need to revise the timeline. (Unless, of course, the PCs decisively resolve the temple once and for all. In that case, I can drop it from my Campaign Status document entirely.)