**From:** http://thealexandrian.net/wordpress/8015/roleplaying-games/node-based-scenario-design-part-6-alternative-node-design

**Title:**Node-Based Scenario Design – Part 6: Alternative Node Design

![The Alexandrian splash](http://pnprpg.ru/wp-content/uploads/2014/08/the_alexandrian_splashes.png)

С оригиналом статьи Вы можете ознакомиться [здесь](http://thealexandrian.net/wordpress/8015/roleplaying-games/node-based-scenario-design-part-6-alternative-node-design)
	
	On the other hand, there is something to be said for the Big Conclusion. There are plenty of scenarios that don’t lend themselves to a shuffling between nodes of equal importance: Sometimes Dr. No’s laboratory is intrinsically more important than Strangeways’ library. And the Architect’s inner sanctum requires the Keymaker.
	
	So let’s talk about some alternative node structures.
	
	CONCLUSIONS
	
	Node-Based Scenario Design - Conclusions
	
	In this design, each node in the second layer contains a third clue that points to the concluding node D. The function should be fairly self-explanatory: The PCs can chart their own course through nodes A, B, and C while being gently funneled towards the big conclusion located at node D.
	
	(Note: They aren’t being railroaded to D. Rather, D is the place they want to be and the other nodes allow them to figure out how to get there.)
	
	One potential “problem” with this structure is that it allows the PCs to potentially bypass content: They could easily go to node A, find the clue for node D, and finish the adventure without ever visiting nodes B or C.
	
	Although this reintroduces the possibility for creating unused content, I put the word “problem” in quotes here because in many ways this is actually desireable: When the PCs make the choice to avoid something (either because they don’t want to face it or because they don’t want to invest the resources) and figure out a way to bypass it or make do without it, that’s almost always the fodder for an interesting moment at the gaming table in my experience. Nor is that content “wasted” — it is still serving a purpose (although its role in the game may now be out of proportion with the amount of work you spent prepping it).
	
	Therefore, it can also be valuable to incentivize the funneling nodes in order to encourage the PCs to explore them. In designing these incentives you can use a mixture of carrots and sticks: For example, the clue in node A might be a map of node D (useful for planning tactical assaults). The clue in node B might be a snitch who can tell them about a secret entrance that doesn’t appear on the map (another carrot). And node C might include a squad of goons who will reinforce node D if they aren’t mopped up ahead of time (a stick).
	
	(That last example also shows how you can create multi-purpose content. It now becomes a question of how you use the goon squad content you prepped rather than whether you’ll use it.)
	
	FUNNELS
	
	The basic structuring principles of the conclusion can be expanded into the general purpose utility of funneling:
	
	Node-Based Scenario Design - Funnels
	
	Each layer in this design (A, B, C and E, F, G) constitutes a free-form environment for investigation or exploration which gradually leads towards the funnel point (D or H) which contains the seeds leading them to the next free-form environment.
	
	I generally find this structure useful for campaigns where an escalation of stakes or opposition is desirable. For example, the PCs might start out investigating local drug dealers (A, B, C) in an effort to find out who’s supplying drugs to the neighborhood. When they identify the local distributor (D), his contacts lead them into a wider investigation of city-wide gangs (E, F, G). Investigating the gangs takes them to the Tyrell crime family (H), and mopping up the crime family gets them tapped for a national Mafia task force (another layer of free-form investigation), culminating in the discovery that the Mafia are actually being secretly run by the Illuminati (another funnel point).
	
	(The Illuminati, of course, are being run by alien reptoids. The reptoids by Celestials. And the Celestials by the sentient network of blackholes at the center of our galaxy. The blackhole consciousness, meanwhile, has suffered a schizord bifurcation due to an incursion by the Andromedan Alliance…. wait, where was I?)
	
	In particular, I find this structure well-suited for D&D campaigns. You don’t want 1st-level characters suddenly “skipping ahead” to the mind flayers, so you can use the “chokepoints” or “gateways” of the funnel structure to move them from one power level to the next. And if they hit a gateway that’s too tough for them right now, that’s OK: They’ll simply be forced to back off, gather their resources (level up), and come back when they’re ready.
	
	Another advantage of the funnel structure is that, in terms of prep, it gives you manageable chunks to work on: Since the PCs can’t proceed to the next layer until they reach the funnel point, you only need to prep the current “layer” of the campaign.
	
	LAYER CAKES
	
	Node-Based Scenarios Designs - Layer Cakes
	
	A layer cake design achieves the same general sense of progression that a funnel design gives you, but allows you to use a lighter and looser touch in structuring the scenario.
	
	The most basic structure of the layer cake is that each node in a particular “layer” gives you clues that lead to other nodes on the same layer, but also gives you one clue pointing to a node on the second layer. Although a full exploration of the first layer won’t give the PCs three clues to any single node on the second layer, they will have access to three clues pointing to a node on the second level. Therefore, the Inverted Three Clue Rule means that the players will probably get to at least one node on the second layer. And from there they can begin collecting additional second layer clues.
	
	Whereas PCs in a funnel design are unlikely to backup past the last funnel point (once they reach node E they generally won’t go back to node C since it has nothing to offer them), in a layer cake design such inter-layer movement is common.
	
	Layer cakes have a slightly larger design profile than funnels, but allow the GM to clearly curtail the scope of their prep work (you only need to prep through the next layer) while allowing the PCs to move through a more realistic environment. You’ll often find the underlying structure of the layer cake arising naturally out of the game world.