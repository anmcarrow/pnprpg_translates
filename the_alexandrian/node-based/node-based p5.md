**From:** http://thealexandrian.net/wordpress/8008/roleplaying-games/node-based-scenario-design-part-5-plot-vs-node

**Title:** Node-Based Scenario Design – Part 5: Plot vs. Node 

![The Alexandrian splash](http://pnprpg.ru/wp-content/uploads/2014/08/the_alexandrian_splashes.png)

С оригиналом статьи Вы можете ознакомиться [здесь](http://thealexandrian.net/wordpress/8008/roleplaying-games/node-based-scenario-design-part-5-plot-vs-node)
	
	As I noted before, the plotted approach gives control to the designer of the scenario by taking that control away from the players.
	
	For example, if we were to re-design our Las Vegas CTU scenario using the plotted approach, we could carefully control the flow of events:
	
	Node-Based Scenario Design - Plotted Approach
	
	Evidence at the self-storage facility (BLUE NODE) leads the PCs to Yassif Mansoor’s apartment (NODE A) where they have a frenzied gun-fight with the suicide bombers. But Mansoor is missing and there’s evidence of an even bigger bomb somewhere in Vegas! Their only hope is to track down the corrupt cop Frank Nasser (NODE C) and force him to break under questioning. But will they reach the Bellagio in time (NODE B)?
	
	This is obviously an effective way for the scenario to play out: Everything builds naturally up to a satisfying confrontation with the Bad Guy and his Big Bomb. The argument can certainly be made that you would want to enforce this linearity to make sure that the PCs don’t take out Mansoor and the Big Bomb half-way through the scenario and end up with a massive anti-climax.
	
	But in making that argument, I think we’re overlooking some equally viable alternatives.
	
	For example: Following evidence at the self-storage facility the PCs head to the Bellagio. There they capture the internationally infamous terrorist Yassif Mansoor and disarm the Big Bomb. It looks like they’ve wrapped everything up, but then they discover the truth: There are more bombs! The Bellagio bombing was only the tip of the iceberg, and even from behind bars Mansoor is about to turn the Las Vegas Strip into a Boulevard of Terror!
	
	(And in a more reactive scenario, you might even introduce the possibility of the undiscovered Nasser somehow freeing Mansoor from his cell.)
	
	My point here is that when you create individually interesting nodes, you’ll generally find that those nodes can be shuffled into virtually any order and still end up with an interesting result. The PCs might even decide to split up and pursue two leads at the same time (in true CTU style).
	
	As a GM I find these types of scenarios more interesting to run because I’m also being shocked and surprised at how the events play out at the gaming table. And as a player I find them more interesting because I’m being allowed to make meaningful choices.
	
	Of course, the argument can be made that there’s no “meaningful choice” here because there are three nodes in the scenario and the PCs are going to visit all three nodes no matter what they do. In the big picture, the exact order in which they visit those nodes isn’t meaningful.
	
	Or is it?
	
	Even in this small, simple scenario, the choices the PCs make can have a significant impact on how events play out. If they go to the Bellagio after they’ve identified exactly which room Yassif Mansoor is in, for example, they’ll have a much easier time of confronting the terrorists without tipping them off. If they have to perform a major search operation on the hotel, on the other hand, the terrorists may have laid a trap for them; Mansoor might have a chance to escape; or there might have been time to make a phone call and warn the terrorists back at the apartment.
	
	And in more complex scenarios, of course, there will be more meaningful contexts for choices to be made within. For example, something as simple as adding a timeline to our sample scenario can make a big difference: If CIA communication intercepts indicate a high threat of a terrorist attack in Vegas timed for 6:00 pm, then PCs standing in the storage facility with multiple leads to pursue suddenly have a tough choice to make. Do they go to the Bellagio because it’s a high-profile target despite the fact they aren’t sure exactly what they should be looking for there? Do they go to Mansoor’s apartment in the hope that they can find out more information about the Bellagio attack? Do they split up and pursue both leads in tandem? Do they call in the Vegas police to evacuate the Bellagio while they pursue the apartment lead? (Is that even a safe option if they have evidence pointing to a mole inside the department?)
	
	Furthermore, when an individual scenario is placed within the context of a larger campaign it allows the choices made within that scenario to have a wider impact. For example, if they tip off Mansoor and allow him to escape by rushing their investigation, it means that he can return as a future antagonist. Similarly, if they don’t find or follow the leads to Nasser, the continued presence of a terrorist-compromised mole in the Las Vegas police force can create ongoing problems for their investigations.
	
	(And if the Bellagio gets blown up there will obviously be a long-lasting impact on the campaign.)
	
	On a more basic level, in my opinion, the fact that the players are being offered the driver’s seat is meaningful in its own right. Even if the choice doesn’t have any lasting impact on the final conclusion of “good guys win, bad guys lose”, the fact that the players were the ones who decided how the good guys were going to win is important. If for no other reason than that, in my experience, it’s more fun for everybody involved. And more memorable.