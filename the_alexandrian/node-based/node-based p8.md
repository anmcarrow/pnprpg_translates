**From:** http://thealexandrian.net/wordpress/8044/roleplaying-games/node-based-scenario-design-part-8-freeform-design-in-the-cloud

**Title:**Node-Based Scenario Design – Part 8: Freeform Design in the Cloud 

![The Alexandrian splash](http://pnprpg.ru/wp-content/uploads/2014/08/the_alexandrian_splashes.png)

С оригиналом статьи Вы можете ознакомиться [здесь](http://thealexandrian.net/wordpress/8044/roleplaying-games/node-based-scenario-design-part-8-freeform-design-in-the-cloud)

	Everything we’ve been discussing here are basic, systematic designs. But there’s no reason you need to be symmetrical. Maybe node A has two clues pointing to node B while node C is clue-happy for node A.
	
	Node-Based Scenario Design - Asymmetrical Nodes
	
	On a larger scale, you’ll probably find yourself mashing together lop-sided conglomerations of disparate structures.
	
	For example, a good-sized chunk of my current campaign is based around a general layer cake approach: An interconnected web of criminal organizations allow the PCs to generally make their way up the “chain of command”. But this layer cake naturally funnels towards various sub-conclusions, and I’ve also included loops designed to carry the PCs back to points prior to the various funnels.
	
	That approach may seem jargon-filled, but it’s really just a matter of embracing the fundamentally flexible principles of node-based design, strewing clues liberally, and spot-checking to avoid problem areas.
	
	Looking over my notes for this campaign, I’ve come to think of this as the “cloud”: Dozens of nodes all containing clues and linked to by clues. Even if we discount all the different ways in which the PCs can approach each of these nodes, the complex relationships which emerge from the node structure make literally hundreds of potential outcomes possible.
	
	But I didn’t have to think about that emergent complexity as I was designing the campaign-scale scenario: All I needed to do was design the criminal organization, break it into node-sized chunks, and then lay down the clues necessary to navigate to and from each node.
	
	As I write this, my players are about mid-way through this section of the campaign. It’s been filled with countless surprises for all of us, and these surprises lead me to a final point regarding the strengths of node-based design: It’s flexible in play.
	
	Because each node is, effectively, a modular chunk of material, it becomes very easy to rearrange the nodes on-the-fly. For example, when the PCs raided an enemy compound and wiped out half of their personnel before being forced to pull back, it was very easy for me to look around, grab a different node full of bad guys, and plug them in as reinforcements.
	
	In other words, it was as easy for me to call in the reinforcements as it was for the NPCs to pick up the phone. Node-based design gives you, by default, the scenario-based toolkit I talked about in “Don’t Prep Plots”. And the underlying structural function of that node hadn’t changed: The NPCs still had the same clues to provide that they’d been designed to provide at their previous location.