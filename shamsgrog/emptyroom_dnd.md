**From:** http://shamsgrog.blogspot.ru/2008/05/empty-room-principle.html
**title:** Принцип Пустой комнаты (перевод)
 
А с оригиналом этого текста Вы можете ознакомиться [в блоге автора](http://shamsgrog.blogspot.ru/2008/05/empty-room-principle.html)
С общим списком наших переводов [вы можете ознакомиться здесь](http://pnprpg.ru/translations/).

---

	Here is my Empty Room Principle of D&D:

	D&D is a vehicle for creative input. Logically, therefore, D&D is hindered when the potential for creativity is reduced.

Dungeons & Dragons (D&D) – машина, движимая вашим творчеством.
Логически, из этого следует, что D&D работает хуже, когда творчество представленно в недостаточном колличестве.
	
	Why the Empty Room? The Empty Room is a metaphor, it represents the potential space provided by the designer or author of a game, to be used by the players of that game to exercise some form of personal creative input. 

*А что за "Пустая комната"?* Пустая комната – это метафора, означающая пустое пространство ("белое пятно"), предоставленное создателем игры для использования игроками в качестве места реализации собственного творчества.  
<!--more--> 
	
	Room 212: Empty Room. This room is empty.

---

### Описание: Комната №212 – Комната пуста
	
	Wow, are you kidding me? I purchased this module so that the author could do this? What kind of product is this…this is a rip-off. How uninspired and unoriginal. An empty room. Bah!

Две возможные реакции: 

> Да вы что, издеваетесь? Я ведь уже заплатил за это готовое приключение, так почему же автор подсовывает мне такое? Что за низкопробное... Да это просто ни в какие ворота! Как безвкусно и неоригинально! Фу!  
	
	OR
	
или
	
	Oh goody! Now I have a nice neat area for me to drop in my recently home brewed Giant Frog Mud Golem guarding that sink hole to my Muck and Mire sublevel!
	
> Ух ты! Отличная возможность вставить сюда моего вчера придуманного Илистого Жабоподобного Голема, охраняющего провал ведущий к подуровню Гадостей Грязюк! 

*(вторая определённо конструктивней – прим пер.)*

---
	
	The Empty Room is evocative of Old School gaming. It means nothing, it insinuates nothing, it is empty and devoid of description. Does it mean that when the characters enter it, they are in fact in a vacuum? No, it means that the referee must fill in the blanks, creating the room’s description to whatever extent he so desires. It’s an invitation for creativity. 


Подобный приём "пустой комнаты" напоминает мне о старых добрых играх *старой школы*. 

> Вот вам пустой кусок, здесь ничего нет, здесь мы ничем вас не ограничиваем, это место полностью чисто и лишено описания.

Но значит ли это, что персонаж зашедший в такую команту окажется в вакууме? 
Нет, это означает, что Ведущий/рефери должен заполнить эту пустоту, самостоятельно придумать содержимое комнаты исходя из своих собственных чаяний. *Каждая пустая комната – это прямое приглашение к творчеству.*  

	It’s the best analogy I can think of for OD&D’s open ended potential for creativity, limited only by one’s own imagination. OD&D’s gaps should always be taken as creative possibilities, much like empty rooms, not as a detriment. As Gary said in the OD&D afterward “…why have us do any more of your imagining for you?”.

Всё это, мне думается, является наилучшей иллюстрацией расширямости и практически бесконечного творческого потенциала [OD&D](http://ru.rpg.wikia.com/wiki/Original_Dungeons_%26_Dragons), который ограницивается только вашим собственным воображением. 
Или же, словами Гари Гайгэкса (в послесловии к книге правил OD&D) – *"...разве можем мы дать вам больше, чем ваше собственное воображение?"*
	
	Thus, the Empty Room Principle, that D&D is a vehicle for creative input. Any version which limits this creativity is in fact moving away from the Empty Room Principle, and therefore moving away from the purest form of D&D.

Таким образом, Принцип Пустой комнаты есть прямое следствие природы самого D&D. Любые другие принципы ущемлющие ваш творческий потенциал как правило весьма далеки от Принципа Пустой комнаты и, следовательно, весьма далеки от первоначальной идеи D&D.
	
	The Empty Room Principle borrows from a simplified form of Occam’s Razor Principle. Occam’s Razor? Huh? Simply put it says: Pluralitas non est ponenda sine neccesitate, or, Entities should not be multiplied unnecessarily. Wait, that’s not the same thing! Essentially, it is. A stronger form of Occam’s Razor is: If you have two equally likely solutions to a problem, pick the simplest. Or in the only form that takes its own advice: Keep things simple.

#### Принцип же Пустой комнаты, в свою очередь, является одной из упрощённых интерпретаций Бритвы Оккама 

> Что такое Бритва Оккама?   
> Вкратце идея Бритвы Оккама звучит как *"Pluralitas non est ponenda sine neccesitate"*, что значит *"Не стоит преумножать сущности без нужды"*. 

> Более развёрнутая форма выглядит иначе: *"В случае двух возмжных решений одного вопроса, выбирай простейшее"*, что можно свести к краткому *"Проще будь!"*.
	
	To extrapolate upon this Empty Room Principle:
	
	1.Rules are what you make of them, and the limits of D&D are defined absolutely by one’s own imagination 		
	2. D&D is best when engaged, both by player and referee, with critical thought and creativity.
	3. If everything is defined, D&D becomes a session of rote memorization of the rules, with success or failure boiling down to random numbers generated on dice. A situation which can be mimicked by a computer.
	4. OD&D trumps all other versions. It is a vehicle for creative input in ways that modern versions are not. How? By keeping things simple (Thank you William of Occam).

#### Если наложить поверх этого наш Принцип пустой комнаты, мы получим следующее:
1. Правила таковы, каковыми вы сами их сделаете, а границы игры в D&D определяются только границами вашего воображения;
2. Самые лучшие игры в D&D случаются, когда (и у Ведущего и у его игроков) присутствует здравый баланс между критическим мышлением и творчеством;
3. Когда всё определяется только кодексами и правилами (без капли творческого подхода), вся игра сводится только к цитатам из правил, заявкам прописанных в правилах действий и генерации случайных чисел при помощи игральных костей. В таком разрезе вся ситуация становится похожей на какую-нибудь [CRPG](http://ru.rpg.wikia.com/wiki/CRPG).
4. OD&D в этом плане имеет ряд преимуществ над другими версиями D&D. Ведь она с самого начала писалась как машина воображения, тогда как более новые версии писались иначе. В чём же разница, и как OD&D это делает? Ответ просто – OD&D не усложняет *(Славься, Вильям Оккамский!)*. 
		
		I am not going to get up on my soapbox again and attempt to say my preference for the purest form of D&D is the best. Clearly, it is not for everyone. Many, nay MOST players and referees alike enjoy the more defined, in-depth treatment of the modern D&D rules. Maybe someone out there reading this will look at OD&D differently because of this post, and this Empty Room Principle, maybe not. What’s truly important here, is that OD&D should never be thought of as primitive or outdated.

Нет, в этом месте я не хочу встать на трибуну и начать вопить, что OD&D есть лучшее из лучшего. Мягко говоря, это высказывание верно далеко не для всех.

Множество, наверное даже большинство Ведущих и игроков получают больше удовольствия от сильнее регламенитрованных и более опекающих правил современных нам версий Dungeons & Dragons.

Однако, может быть, после этого моего опуса, кто-нибудь взглянет на OD&D иначе, и воспримет этот Принцип Пустой комнаты с должной симпатией. *А может быть и нет.*
Что на само деле важно, так это то, что OD&D точно не заслуживает своей репутации примитивной и в корне устаревшей системы правил.   

	It’s simply the purest form of D&D, and one which should be the acknowledged basis for all other versions. After all, every later edition of the game is simply a heavily house ruled version.

На самом деле, OD&D – просто самая чистая (пуристская) из форм D&D, и, в этом качестве должна бы быть базой основных принципов для всех остальных версий. 
Ведь, насколько мы видим, Ведь, в конце концов, каждая последующая редакция D&D – это всего лишь более-менее объёмный набор чьих-то *"домашних правил"*. 
	
	Now, go out and fill some empty rooms!

Ну а теперь, идите и наполните своим воображением какую-нибудь пустую комнату!