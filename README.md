![](https://www.dropbox.com/s/cpbylqv5n6wwext/banner_200x200.png?dl=1)

В этом Git-репозитории представлены исходные тексты (в формате [Markdown](https://ru.wikipedia.org/wiki/Markdown))
переводов различных англоязычных статей для настольно-ролевого блога [PnPRPG.ru](http://pnprpg.ru).

Все тексты переводов распространяются под лиценизией [Creative Commons Share-Alike v 3.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.ru), если не указано иного.

Все оригиналы переведённых статей принадлежат их авторам и были использованы с их согласия.

Обновлённую версию репозитория вы всегда можете найти на странице 
<http://gogs.mcrrw.ru/pnprpg/translates>

Если вы хотите как-либо дополнить или исправить наши переводы, просто сделайте commit в отдельную ветку и
отправьте эту ветку на merge request.

#### Список подразделов:

- creaturespotlight — статьи из блога [The Lair](http://creaturespotlight.blogspot.ru/);
- gatherroundrpg — статьи из блога ["Gather 'Round The Table"](http://gatherroundrpg.blogspot.ru/);
- random_tables — таблицы случайных событий/предметов из различных источников;
- roflinitiative — статьи из блога "[ROFL initiative"](http://www.roflinitiative.com/)
- shamsgrog — статьи из блога ["Sham's Grog & Blog"](http://shamsgrog.blogspot.ru/);
- the_alexandrian — Статьи из блога ["The Alexandrian"](http://thealexandrian.net/);
- hillcantons — Статьи из блога ["Hill Cantons"](hillcantons.blogspot.ru);
- other — всё остальное.
