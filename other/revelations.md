**Title:** Откровения (перевод)

**From:** http://arsludi.lamemage.com/index.php/41/revelations/

С оригиналом статьи Вы можете ознакомиться [здесь](http://arsludi.lamemage.com/index.php/41/revelations/).

---

Normal weapons can't kill the zombies. MicroMan doesn't trust Captain Fury. The lake monster is really Old Man Wiggins in a rubber mask.

These are Revelations. They are things you want the players to find out so that they can make good choices or just understand what is going on in the game. Revelations advance the plot and make the game dramatically interesting. If the players don't find them out (or don't find them out at the right time) they can mess up your game.

You have revelations in your game all the time whether you think about it or not, but you can improve your game by planning your revelations ahead of time instead of just letting them happen haphazardly. Lame Mage adventures like Dr Null: Battle on the Bay Bridge [free download] have clearly outlined revelations to make the GM's job easier.

Understanding revelations requires you to see things through the eyes of your players – what are they going to think at this point, will this be a surprise, will this seem consistent? As the GM you know lots of things the players don't. After you've spent days preparing a game, it's easy to become so absorbed in the details of your plot that you lose track of how it will unfold for your players.

Write Your Revelations
An important revelation should be a critical point in the game, changing players' perceptions of the situation and possibly their response.

— excerpt from Death of Dr Null

Writing out your revelations ahead of time shows you how the game is going to flow. Once play starts things can get a little hectic – you may accidentally have the evil mastermind show up and deliver his ultimatum and stomp off again without remembering to drop that one key hint that leads the heroes to his base. If you're lucky you recognize the omission and can backtrack. If you're unlucky you don't notice it at all, and you spend the rest of the game wondering why the players have such a different idea of what is going on than you do.

Your list of revelations does not have to be detailed. In fact it's probably better to make each revelation a simple sentence you can absorb at a glance. Just writing “the Duke is now left-handed” on your list is enough: all the other details of who / what / where / why the Duke has been replaced by an impostor can be in the main body of the adventure. It should be a checklist or a trigger, not the whole explanation.

Moreover the revelation is not a canned moment: it doesn't say precisely how the fact is going to be revealed (though you may certainly have plans or expectations of how it's going to happen). This encourages you to remain flexible in-game, not script the whole thing. Players thrive on doing the unexpected, and a bare bones list of revelations lets you go with the flow but still refer back to your critical details. If the players did not go to the masque ball and see the Duke holding his wine glass in his left hand, you can see that “left-handed” is still on your revelations list and work it in some other way.

Notes
Of course the players may figure out more than you expect, sooner than you expect (they're precocious in that way). Your revelations are really the minimum the players should know at any point for the game to work.

Some revelations may be optional, particularly if they are clues that are not important if the PCs jump ahead and figure out the mystery without them. It doesn't matter if the PCs notice the duke is left-handed if they already figured out he's an impostor.

Revelations are also moments in play. The players may have guessed that Doc Oblivion really has an evil twin and may even be joking about it among themselves, but unless they talk about it in-character you still need the revelation moment for them to roleplay their reactions and establish it as a fact the characters know.



*To be continued...*

*[©Ben Robbins, 25.10.2006](http://arsludi.lamemage.com/index.php/41/revelations/)*   