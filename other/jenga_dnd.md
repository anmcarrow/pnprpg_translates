**Оригинал:** http://slyflourish.com/dnd_jenga.html

**Title:** Дженга и D&D-паззлы

by Mike Shea on 18 June 2012

While skill rolls represent the majority of non-combat challenges in D&D, they aren't particularly exciting. Because of this, many DMs have learned how to incorporate mini-games into their adventures to change things up and keep their players interested. Of all popular puzzle games, the game Jenga is quite well known. Today we're going to look at incorporating Jenga into your D&D games to keep things new, exciting, and interesting. Include this in your portfolio of other mini-games such as Strimko puzzles, Sudoku puzzles, Mastermind puzzles and lock-pick mazes. These puzzles can work well in any version of Dungeons and Dragons.

Jenga Heroes

(Jenga Heroes by Jay Wojciechowski, used with permission)

The core rules

The core of our Jenga D&D mini-game is simple. One or more players engages in a contest against an enemy or a construct. This enemy may be an actual entity such as a psionic lich or an ancient spirit haunting the PC's dreams. Instead of an enemy, the challenge might be against an inanimate construct such as an ethereal prison or a powerful arcane lock. In either case, the PCs engage in a challenge against either an enemy or a construct to gain a story advantage.

The basic challenge involves one or more players alternating turns with the DM as they pull out blocks from the Jenga tower. If the tower falls during a player's turn, the players fail. If the tower falls on the DM's turn, the players succeed.

Though it is a completely different game, we want Jenga to fit well into D&D. We also want to give players an advantage over the DM. To do this, we'll use skill checks. A successful skill check forces the DM to pull TWO blocks instead of just one. A failed skill check means the DM only has to pull one block on his or her turn. This gives players an advantage without throwing the game too much in their favor.

If the Jenga contest represents a contest between PCs and a conscious antagonist, each side engages in an opposed roll. For example, if Magdar the Mottled, a wizard of no small power, engages in a psionic contest with Xathar the Demilich, they each roll opposed arcane checks. If Magdar the PC wins the roll, Xathar must draw two blocks on the DM's turn instead of just one.

If the Jenga contest represents a contest between PCs and an inanimate construct such as an ethereal prison, the PCs roll against a static difficulty check. You can use the DM Cheat Sheet to determine an appropriate skill check and use easy, normal, or hard checks based on the circumstances of the situation.

Now we'll get into some example scenarios that might use this Jenga contest.

Scenario: The psionic battle with the ghost of Kalak

A party of Dark Sun adventurers finds themselves in a battle with the ghost of Kalak, the sorcerer king. While the physical battle takes place, a psionic battle also wages. As a minor action, any PC may engage Kalak in a psionic duel. When the PC takes this action, he or she rolls an arcana or endurance check against Kalak's skill roll of +14 arcana. Regardless of the result, the player pulls a block from the Jenga tower. If the player won the skill roll, the DM now draws two blocks. If the player lost the skill roll, the DM only has to pull one block. After the pull, the battle returns to normal.

If the tower falls on the player's draw, the ghost of Kalak grows stronger, gaining +5 damage to all damage rolls. If the tower falls on the DM's draw Kalak's ghost is overcome by the PCs psionic attacks and gains -4 to all defenses.

Scenario: The arcane lock

The arcane lock is a simpler challenge used to represent a powerful wizard's lock. In this challenge the players are free to choose who tries to break the lock each turn. Like the primordial prison, the lock has a standard DC appropriate for the level. If the lock is particularly difficult, you might use a hard DC. Each player who chooses to alternates with the DM drawing blocks. Like before, a successful skill roll against the lock forces the DM to draw two blocks.

If the lock collapses when a player is drawing a block, the lock became unstable and all PCs take 20 damage per tier in unstable arcane energy feedback. If the lock collapses on the DMs turn, the PCs were able to successfully disarm the lock without any negative consequence.

Scenario: The intelligent sword

This final example represents a challenge between a single PC and an egotistical intelligent weapon. Unlike the other challenges, this one focuses on a single player versus the DM as the PC and the weapon battle for dominance over one another. In this example, the Jenga tower represents the combined ego of both the PC and the intelligent weapon.

Like the arcane lock, there is a fixed DC based on the level of the weapon. If the weapon is rare or an artifact you might choose a hard DC. Each turn, the player rolls a diplomacy, bluff, intimidate, endurance, or arcane check against the sword's DC. The player then draws a block from the Jenga tower. On the DM's turn, the DM must draw two blocks if the player's skill roll was successful or only one if the player's skill roll failed.

If the tower falls on the player's turn, the weapon dominated the ego of the PC and may influence the PC's actions. If the tower falls on the DM's turn, the player successfully dominated the weapon and has full access to its powers without any detrimental effect.

These are just a few potential examples for using Jenga in your game. Mixed with a variety of other puzzles and mini-games, you can keep your game fresh, exciting, and interesting.

If you enjoyed this article, you might enjoy the Lazy Dungeon Master. You can also support this site by using these links to purchase the D&D Starter Set, Players Handbook, Monster Manual, or Dungeon Master's Guide. Send feedback to @slyflourish on Twitter or email mike@mikeshea.net.