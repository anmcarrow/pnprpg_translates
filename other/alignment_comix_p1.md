# Краткий гид по мировоззрениям, ч.1 (перевод)

![goods_1](https://lh4.googleusercontent.com/-7w721wKC9yw/U94kpRH7BzI/AAAAAAAAH_Y/1WsB8uMw13Y/s500-no/good_1.png)

Всего-лишь небольшой перевод комикса про [D&D Alignments](http://ru.rpg.wikia.com/wiki/Мировоззрение), из блога [NoobTheLoser](http://noobtheloser.tumblr.com/tagged/alignment), часть 1 из 3.

---
<!--more-->

![goods_2](https://lh3.googleusercontent.com/-S9ZmiJXtruA/U94kpQHqx-I/AAAAAAAAH_k/15re7fklBxE/s500-no/good_2.png)

![goods_3](https://lh3.googleusercontent.com/-uUbT3-2f7w4/U94kpW6wSpI/AAAAAAAAH_c/zKdbwgjNiAo/s500-no/good_3.png)

![goods_4](https://lh3.googleusercontent.com/-78rFPLrorio/U94kp4pwUbI/AAAAAAAAH_o/1teTye2KV_Y/s500-no/good_4.png)

![goods_5](https://lh4.googleusercontent.com/-YPhYYkbciUw/U94kqCPQIrI/AAAAAAAAH_E/2PVEydy__kg/s500-no/good_5.png)

![goods_6](https://lh3.googleusercontent.com/-L7iGyIsWiAQ/U94kqRtG38I/AAAAAAAAH_Q/uwrfnT69Xfg/s500-no/good_6.png)

![goods_7](https://lh5.googleusercontent.com/-fDSdyTxKv0M/U94kqs59bCI/AAAAAAAAH_M/PuijUcn7qPU/s500-no/good_7.png)

![goods_8](https://lh6.googleusercontent.com/-jBIVA3sEQPw/U94kq00tN_I/AAAAAAAAH_U/33O_SqDfazc/s500-no/good_8.png)

*To be continued...*