**From:** http://www.gnomestew.com/tools-for-gms/my-improv-game-setup-a-photo-post/


On my last post about running an Improv game, Toldain asked if I could share my setup for Improv games. Since I run a lot of convention games, I thought the best way to share the setup would be to do a photo filled post showing exactly what is in my setup.

It all starts with a box . . .

THE BOX

100_2117

I usually carry a courier bag to conventions, but I found it just wasn’t cutting it as far as keeping stuff organized. While looking for a good setup I stumbled across the idea of file boxes.  File boxes are the perfect size for gaming books and papers. Almost anything else, if the right type is gathered, can fit inside of it. Plus, the file box has another benefit.

FOLDERS

100_2120

Yup, folders. They make it awesome and easy to keep things organized in the box. You can pull a folder out, find what you need, then put it back in. I’m going to go through the rest of the physical contents by their individual folders.

Office Supplies and Dice

100_2122

Folder #1 is  all about the essentials. I keep bags of dice and boxes, if they are thin enough, in this folder. I keep them with enough to run in most systems. A couple of sets of d10s, a couple of full polyhedral sets, and a big bag of d6s, including some fudge dice. I also keep a little pencil bag full of pencils and markers, especially dry erase markers. They fit nicely in the folder and don’t just fly around when I’m carrying the box.

Paper and Notes

100_2125

This might actually be the MOST IMPORTANT thing in my improv box. I keep at least one spiral bound notebook for taking notes while running the game and I also keep a bunch of sheets of scrap paper that I can use. I can write down initiative order, make nametags, draw quick things out that I can’t describe adequately, make maps if I don’t have anything else, keep whatever notes don’t fit into stat blocks, etc.

Cheat Sheets

100_2127
Ok, this one is a little specific to me and the company games I usually run. I try to apply the concept to any game though. If possible, I try to keep a cheat sheet for any game I run. That way if the players (or I) can’t remember the basics it doesn’t require the full book. If I can’t remember the exact measurements for savage worlds gun ranges the cheat sheet will have it. If I can find the cheat sheets online, awesome! If I’m making them myself, then it is usually geared towards the game styles I run.

One big thing I try to keep in my cheat sheets folder are generic stat blocks and npc types. Some games make this easier than others, but I want one or two pages with just enough information about 4 or 5 types of enemies. That way I can reskin it on the fly. The big bruiser for a modern game can become the orc for a fantasy game or the warrior alien for a sci-fi game with a bit of tweaking.

Maps

 100_2129

Ok, I like maps, even if I’m not running gridded combat. Maps for an improv setup can be pretty easy, they just have to fit into the space of a file folder. Map tiles are great. Foldable maps are great. Gaming paper is great. I try to keep a few different types in here, and I switch them out when I know the types of games I’m running are changing. I just picked up a really awesome ships pack from Paizo, this saves me loads of time as ships and airships factor into a lot of my games. Specialty map tiles like this are great because the specialty maps can’t be as easily recreated through other means. Hallways, simple. Spaceship, harder. I tend to keep the map tiles in mailing envelopes that just fit the size. They aren’t hard to find and they keep the tiles nice and organized, something necessary for an on the go setup.

100_2131

Character Sheets/Major NPCs

 100_2134

I’ve got a folder full of pregenerated characters. The ones that get most used I had laminated. I have them for a few different systems, and they serve as characters for players who don’t have characters or major NPCs if I need something more fleshed out than a cheat sheet stat block will handle.

Books

 100_2139

In separate folders I keep quite a few books, some are perennial and some get switched out. The Story Games Names Project book is always in there, as is the One Page Dungeon Codex, and Besm Dungeon. The Story Games Names Project book is essential to me, it is a book full of names in many many categories. E-fracking-ssential, as I’m horrible with coming up with names on the fly.

100_2136

The One Page Dungeon Codex is great for “I need a dungeon quick”, and so is Besm Dungeon. They both cut it down to just the essentials to run a dungeon. You can easily reskin and use the maps as templates for quick adventures or just run off of them as written with whatever system you are running.

100_2140 100_2138

Shameless Self Promotion Books

100_2142

Ok, I’ve got to disclaim that I’m a writer for both of these books, but they are two that are always in my gaming box. The Silvervine book is always there because it is the system I run most at conventions and, in my completely biased opinion, is very easy to run improv games with. The stat blocks I talked about earlier, yeah, stat blocks like these make it easy to run combats.

image

To run a really easy improvised combat, work out a generic stat block for the archetypes of enemies that exist. Dodgy, Damage Dealers, Hard to Damage, etc. They may not be 100% accurate to a specific enemy, but they’ll get you through most of what you need.

Then of course there is Eureka. If I’m stuck for a plot, I pull this open and find one based on the tags in the back. It’s incredibly easy to pull a plot out of my as….this book and just run with it. Ok, no more shameless self promotion.

The Box O’ Toys

100_2146

Ok, these are my minis and other things for improv games. I keep one box, of the type you can find at craft stores for beads or fishing stores for holding lures. They hold minis incredibly well and let you section them off a bit. If you can find one like this (~$4) you get one big side compartment to hold big things. Here is what I keep in mine.

Tokens and Zombies
100_2146

I keep a bunch of tokens, per Patrick’s Not Just a Token Gift article, which was modified from one Newbie DM did. Thanks Newbie DM! You really made my improv box with that article. The beautiful thing about these tokens is that you can do so much with them and they take up so little room. I try to have pictures that fit pregens I know I have, but I also do multiple enemies. The zombies are there because they make great mass enemies. I can stick a mini of an Orc up front and put 8 zombies behind it. I say “The zombies behind the orc are all orcs as well.” and we have minis. I can also use the tokens with the alphabet on the back and just stick those behind a mini to represent more of the same.

Hero and Enemy Minis100_2146

I try to keep minis that represent the basic hero and enemy types. Using the “more of the same” method above, they can easily be duplicated in the players minds. With a small collection, a perfect match isn’t going to be possible but a “close enough” might be. I generally find that tokens are ignored for 3D minis, but I think that is just because we gamers like our toys.

Counters and “Inanimates”100_2146

I try to keep a couple of full sized and mini sized poker chips in a compartment for whatever might come up. Full sized ones are great for denoting conditions. Mini ones are great for generic enemies or any one space thing. I also try to keep some minis that represent inanimate things. Dollhouse furniture, small statues, etc. These really help when I need to give some feeling and a bit of 3dness to the map.

Quick Walls and Doors
100_2146

Jenga Blocks, Dominoes, and doors if you can find them. These are the key to my improv maps. Buy a set of Jenga blocks and you’ve got decent walls. Find a set of wooden dominoes and you’ve got twice as many. Use them in tandem and you can build all sorts of structures. The doors here are from an old mage knight set, but an upright domino in-between Jenga blocks works perfectly. Bonus – If you have a full set of blocks you can run dread.

The Second Box!!!!!

Ok, sometimes I’ll grab a second box full of terrain and more minis. This is usually tailored towards whatever games I know I’m running, but I try to keep it around with enough to adapt. I usually use a bucket for this box, seeing as how it fits things much nicer and doesn’t need to be as organized. In this box I usually keep a more robust set of minis, as many pieces of Heroscape terrain as I can manage neatly, and a full Jenga block set. Plastic pencil boxes help me keep it organized.

100_2149

100_2164 100_2163


The Robust Set of Minis

100_2150
Without the need to generalize and conserve space, I can store more things in here. I try to tailor it to my games but there are a few general rules I follow.

100_2151 Keep one or two big minis, like Cthulu here, to represent big monsters.
Keep 2 or 3 minis of the same type to represent the same types of creatures or to have one mini at the head of a group of counters that represent more of the same.
Keep a decent group of hero minis.
Keep counters of multiple types.
If you can have inanimate objects, great.
You’ll never have enough minis to represent everything, so look for good enoughs.
100_2153


Ok, With All That Laid Out, You Can Get Some Pretty Decent Setups

The Terrain doesn’t need to be perfect, just representative.100_2154

Here is an example of more of the same. The stormptroopers on the tower are being represented by the 3 tokens as well as the minis.
 100_2155

The white tokens under the gremlins represent swimming penalties.
100_2156

The walls don’t NEED to be built all the way around, vertical blocks and a note in dry erase on the map convey enough information.
100_2157 100_2158



And In The End It Fits Right Back Into Two Neat Boxes100_2161

One If You Aren’t Looking To Do An Impressive Setup For Terrain100_2162


One Piece Of Advice To Leave You With
You don’t need any of this stuff.

I focused on the physical aspects of this because these are the things that help me run improv games, but really it is mostly unnecessary. This setup helps me account for any situation with the polish of a more prepared game, and sometimes that really makes a difference, but really all you need to run an improv game is paper, pencils, and whatever system you are running in. The rest is all gravy. Some of the most fun improvised games I’ve run have been done with very little in the way of supplies. If you want one or two tools that make that possible go for blank paper and a writing instrument. With those two things anything else can be produced in some way. I’ve seen entire setups done with stick figure minis and drawn on the fly maps.

So, you’ve seen my setup. What about yours? What tools do you keep in your GMing kit for improving or otherwise? I’m always looking for, as I’m sure every other GM is, new things to keep in my GM kit. What do you find indispensible in yours?