# Краткий гид по мировоззрениям, ч.3 (перевод)

![evils_1](https://lh6.googleusercontent.com/-HM-W5CgMpcg/U96vS97xt_I/AAAAAAAAIAc/-BOedfdE5Ew/s500-no/evil_1.png)

Всего-лишь небольшой перевод комикса про [D&D Alignments](http://ru.rpg.wikia.com/wiki/Мировоззрение), из блога [NoobTheLoser](http://noobtheloser.tumblr.com/tagged/alignment), часть 3 из 3.

---
<!--more-->

![evils_2](https://lh5.googleusercontent.com/-diJpwryXZMQ/U96vTMV3C5I/AAAAAAAAIBQ/smU-rl8CkT4/s500-no/evil_2.png)

![evils_3](https://lh4.googleusercontent.com/-z-V-kBZ6_Rw/U96vTB2OqVI/AAAAAAAAIAo/RLeDHZRgdQE/s500-no/evil_3.png)

![evils_4](https://lh5.googleusercontent.com/-2eYHpfasiI0/U96vTuxtV1I/AAAAAAAAIAw/_MC9Duyix1s/s500-no/evil_4.png)

![evils_5](https://lh5.googleusercontent.com/-tTh6DKDzzro/U96vT9GyxWI/AAAAAAAAIBI/RcgIab7SzoI/s500-no/evil_5.png)

![evils_6](https://lh4.googleusercontent.com/-yTCgSpnZ7EE/U96vUTVDnlI/AAAAAAAAIBA/8Km3b7BV1eo/s500-no/evil_6.png)

![evils_7](https://lh5.googleusercontent.com/-iSJw8yGpVbo/U96vUm4nOxI/AAAAAAAAIA8/tF3VhlzwHoQ/s500-no/evil_7.png)

![evils_8](https://lh4.googleusercontent.com/-89o3qNBu8P4/U96vVJnQiHI/AAAAAAAAIBE/WuX5Q00rbK0/s500-no/evil_8.png)

*This is the end, my only friend, the end...*