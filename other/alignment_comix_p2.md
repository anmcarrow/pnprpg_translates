# Краткий гид по мировоззрениям, ч.2 (перевод)

![neutrals_1](https://lh3.googleusercontent.com/-GOE6tgo6OLs/U94l7qXrnhI/AAAAAAAAH_8/E-wuKUjGvek/s500-no/neutral_1.png)

Всего-лишь небольшой перевод комикса про [D&D Alignments](http://ru.rpg.wikia.com/wiki/Мировоззрение), из блога [NoobTheLoser](http://noobtheloser.tumblr.com/tagged/alignment), часть 2 из 3.

---
<!--more-->

![neutrals_2](https://lh6.googleusercontent.com/-5fGe0K_nwjU/U94YViOWgWI/AAAAAAAAH9s/AzN0jabQToA/s500-no/neutral_2.png)

![neutrals_3](https://lh3.googleusercontent.com/-xnAL5najweI/U94YVv0LofI/AAAAAAAAH9k/Wg2RgiiTPj4/s500-no/neutral_3.png)

![neutrals_4](https://lh4.googleusercontent.com/-KR_4XgXsrZs/U94YWInyuLI/AAAAAAAAH9g/NQaxl9r38oE/s500-no/neutral_4.png)

![neutrals_5](https://lh4.googleusercontent.com/-6Os8eAbmnhA/U94YWWk7ybI/AAAAAAAAH9M/Mo7H1r5xPWQ/s500-no/neutral_5.png)

![neutrals_6](https://lh3.googleusercontent.com/-n8tRG56D5Xs/U94YWkfD0EI/AAAAAAAAH9U/_Jg1bqQrnco/s500-no/neutral_6.png)

![neutrals_7](https://lh3.googleusercontent.com/-OkRyI9aVZWI/U94YXGKtu1I/AAAAAAAAH9Y/MtS_Mxsqq54/s500-no/neutral_7.png)

![neutrals_8](https://lh3.googleusercontent.com/-ajcxgbGNGHE/U94YXRQqE9I/AAAAAAAAH9c/tQMsbdn38o8/s500-no/neutral_8.png)

*To be continued...*